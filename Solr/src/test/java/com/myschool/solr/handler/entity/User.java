/*
 * @(#)User.java 2014-12-19 下午4:23:27
 * Copyright 2014 鲍建明, Inc. All rights reserved. 8637.com
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myschool.solr.handler.entity;

import java.io.Serializable;

import org.apache.solr.client.solrj.beans.Field;

/**
 * <p>File：User.java</p>
 * <p>Title: </p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2014 2014-12-19 下午4:23:27</p>
 * <p>Company: 8637.com</p>
 * @author 鲍建明
 * @version 1.0
 */
public class User implements Serializable
{
	@Field("id")
	private String id;
	@Field("age")
	private Integer age;
	@Field("name")
	private String name;
	
	
	public User(){}
	
	

	/**
	 * @param id
	 * @param age
	 * @param name
	 */
	public User(String id, Integer age, String name)
	{
		super();
		this.id = id;
		this.age = age;
		this.name = name;
	}



	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public Integer getAge()
	{
		return age;
	}

	public void setAge(Integer age)
	{
		this.age = age;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [id=" + id + ", age=" + age + ", name=" + name + "]";
	}
	
	
	
}
