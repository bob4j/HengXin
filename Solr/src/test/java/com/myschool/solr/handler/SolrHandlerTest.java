/*
 * @(#)SolrHandlerTest.java 2014-12-19 下午4:21:19
 * Copyright 2014 鲍建明, Inc. All rights reserved. 8637.com
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myschool.solr.handler;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.common.params.CommonParams;
import org.junit.Test;

import com.myschool.solr.bean.PageResult;
import com.myschool.solr.handler.entity.User;
import com.myschool.solr.query.QueryBean;

/**
 * <p>File：SolrHandlerTest.java</p>
 * <p>Title: </p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2014 2014-12-19 下午4:21:19</p>
 * <p>Company: 8637.com</p>
 * @author 鲍建明
 * @version 1.0
 */
public class SolrHandlerTest
{

	/**
	 * Test method for {@link com.myschool.solr.handler.SolrHandler#addByBean(java.util.List)}.
	 * @throws Exception 
	 */
	@Test
	public void testAddByBeanListOfQ() throws Exception
	{
		SolrHandler handler = new SolrHandler("http://localhost:8081/solr", "/core0");	
		List<User> list = new ArrayList<User>();
		for (int i = 0; i < 1000; i++) {
			
			list.add(new User("2" + i, 23 + i, "小王" + i));
		}
		handler.addByBean(list);
		
	}

	/**
	 * Test method for {@link com.myschool.solr.handler.SolrHandler#addByBean(java.lang.Object)}.
	 * @throws Exception 
	 */
	@Test
	public void testAddByBeanObject() 
	{
		SolrHandler handler = new SolrHandler("http://localhost:8081/solr", "/core0");				//选择索引库core0
		try {
			handler.addByBean(new User("1", 23, "小王"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link com.myschool.solr.handler.SolrHandler#addByDocument(java.util.Map)}.
	 * @throws Exception 
	 */
	@Test
	public void testAddByDocument() throws Exception
	{
		SolrHandler handler = new SolrHandler("http://localhost:8081/solr", "/core0");			//其实这种document生成的方式就是bean生成方式的底层方式 ，只不过bean生成的时候先转成document的
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", "map1");
		params.put("name", "我是document生成的");
		params.put("age", 99);
		handler.addByDocument(params);
	}

	/**
	 * Test method for {@link com.myschool.solr.handler.SolrHandler#deleteById(java.lang.String)}.
	 * @throws Exception 
	 */
	@Test
	public void testDeleteByIdString() throws Exception
	{
		SolrHandler handler = new SolrHandler("http://localhost:8081/solr", "/core0");
		handler.deleteById("1");
	}

	/**
	 * Test method for {@link com.myschool.solr.handler.SolrHandler#deleteById(java.util.List)}.
	 * @throws Exception 
	 */
	@Test
	public void testDeleteByIdListOfString() throws Exception
	{
		SolrHandler handler = new SolrHandler("http://localhost:8081/solr", "/core0");
		List<String> ids = new ArrayList<String>();
		ids.add("1");
		handler.deleteById(ids);
	}

	/**
	 * Test method for {@link com.myschool.solr.handler.SolrHandler#update(java.lang.String, java.lang.Object)}.
	 * @throws Exception 
	 */
	@Test
	public void testUpdate() 
	{
		SolrHandler handler = new SolrHandler("http://localhost:8081/solr", "/core0");
		try {
			handler.update("1", new User("1", 4423, "小王333"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link com.myschool.solr.handler.SolrHandler#queryResult(com.myschool.solr.query.QueryBean)}.
	 * @throws Exception 
	 */
	@Test
	public void testQueryResult() throws Exception
	{
		SolrHandler handler = new SolrHandler("http://localhost:8081/solr", "/core0");
		QueryBean q = new QueryBean(User.class);
		SolrQuery query = q.getQ();
		//query.add(CommonParams.Q, "*:*");   等同于new SolrQuery("*:*")
		query.add(CommonParams.Q, "id:1");
		List<String> fields = new ArrayList<String>();
		fields.add("name");
		q.setHighilight(true, fields);
		//q.setQ("name:*王*,id:23");
		PageResult<User> result = handler.queryResult(q);
		//查询相关的一些，可以在http://localhost:8081/solr中去尝试，他都有的。在他的Query选项中。 
		
		System.out.println(result.getPage());
		for (User u : result.getResult()) {
			System.out.println(u);
		}
		
		
		
	}

	/**
	 * Test method for {@link com.myschool.solr.handler.SolrHandler#addByFile(java.io.File, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testAddByFile()
	{
		fail("Not yet implemented");
	}

}
