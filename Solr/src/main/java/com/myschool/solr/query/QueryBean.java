/*
 * @(#)QueryBean.java 2014-12-19 下午3:30:10
 * Copyright 2014 鲍建明, Inc. All rights reserved. 8637.com
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myschool.solr.query;

import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;

import com.myschool.solr.bean.Highilight;
import com.myschool.solr.bean.Page;

/**
 * <p>File：QueryBean.java</p>
 * <p>Title: </p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2014 2014-12-19 下午3:30:10</p>
 * <p>Company: 8637.com</p>
 * @author 鲍建明
 * @version 1.0
 */
public class QueryBean 
{
	private SolrQuery q;			//你也可以选择使用对象的形式
	private Page page;
	private Highilight highilight ;
	private Class<?> type;
	
	
	
	public QueryBean(Class<?> type){
		this.page = new Page();
		this.highilight = new Highilight();
		this.type = type;
		this.q = new SolrQuery();
	}
	
	
	public Page getPage()
	{
		return page;
	}
	public void setPage(Page page)
	{
		this.page = page;
	}
	public Highilight getHighilight()
	{
		return highilight;
	}
	public void setHighilight(Highilight highilight)
	{
		this.highilight = highilight;
	}
	public Class<?> getType()
	{
		return type;
	}
	public void setType(Class<?> type)
	{
		this.type = type;
	}
	public SolrQuery getQ()
	{
		return q;
	}
	public void setQ(SolrQuery q)
	{
		this.q = q;
	}
	public void setHighilight(Boolean isHighilight, List<String> fields)
	{
		this.highilight.setIsHighilight(isHighilight);
		this.highilight.setFields(fields);
	}
	
	
}
