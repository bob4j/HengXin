/*
 * @(#)PageResult.java 2014-12-19 下午3:11:01
 * Copyright 2014 鲍建明, Inc. All rights reserved. 8637.com
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myschool.solr.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.response.QueryResponse;

/**
 * <p>File：PageResult.java</p>
 * <p>Title: </p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2014 2014-12-19 下午3:11:01</p>
 * <p>Company: 8637.com</p>
 * @author 鲍建明
 * @version 1.0
 */
public class PageResult<T> implements Serializable
{

	//
	private static final long	serialVersionUID	= 9016574825694592616L;

	/**
	 * 分页集
	 */
	private Page page;
	/**
	 * 数据Bean集
	 */
	private List<T> result;
	/**
	 * 高亮数据集
	 */
	private Map<String, Map<String, List<String>>> highlighting;
	/**
	 * solr原生返回的对象集
	 */
	private QueryResponse response;
	
	public PageResult(){
	}
	
	
	
	/**
	 * @param page
	 * @param result
	 * @param highlighting
	 * @param response
	 */
	public PageResult(Page page, List<T> result,
			Map<String, Map<String, List<String>>> highlighting,
			QueryResponse response)
	{
		super();
		this.page = page;
		this.result = result;
		this.highlighting = highlighting;
		this.response = response;
	}






	public Page getPage()
	{
		return page;
	}
	public void setPage(Page page)
	{
		this.page = page;
	}
	public List<T> getResult()
	{
		return result;
	}
	public void setResult(List<T> result)
	{
		this.result = result;
	}
	public Map<String, Map<String, List<String>>> getHighlighting()
	{
		return highlighting;
	}
	public void setHighlighting(Map<String, Map<String, List<String>>> highlighting)
	{
		this.highlighting = highlighting;
	}
	public QueryResponse getResponse()
	{
		return response;
	}
	public void setResponse(QueryResponse response)
	{
		this.response = response;
	}
	
}
