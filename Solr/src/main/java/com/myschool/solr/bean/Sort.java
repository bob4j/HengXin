/*
 * @(#)Order.java 2014-12-19 下午3:21:38
 * Copyright 2014 鲍建明, Inc. All rights reserved. 8637.com
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myschool.solr.bean;

import org.apache.solr.client.solrj.SolrQuery.ORDER;


/**
 * <p>File：Order.java</p>
 * <p>Title: 排序对象</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2014 2014-12-19 下午3:21:38</p>
 * <p>Company: 8637.com</p>
 * @author 鲍建明
 * @version 1.0
 */
public class Sort
{
	private String field;
	private ORDER order;
	
	public String getField()
	{
		return field;
	}


	public void setField(String field)
	{
		this.field = field;
	}
	
	public ORDER getOrder()
	{
		return order;
	}

	public void setOrder(ORDER order)
	{
		this.order = order;
	}

}
