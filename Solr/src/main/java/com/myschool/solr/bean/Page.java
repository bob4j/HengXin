/*
 * @(#)Page.java 2014-12-19 下午3:09:14
 * Copyright 2014 鲍建明, Inc. All rights reserved. 8637.com
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myschool.solr.bean;

import java.io.Serializable;
import java.util.List;

/**
 * <p>File：Page.java</p>
 * <p>Title: 分页对象</p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2014 2014-12-19 下午3:09:14</p>
 * <p>Company: 8637.com</p>
 * @author 鲍建明
 * @version 1.0
 */
public class Page implements Serializable
{

	//
	private static final long	serialVersionUID	= 922690048619926973L;

	private Integer pageNow = 1;			//没有请默认
	private Integer pageSize = 20;
	private Long rowSize;
	private Integer totle;
	private List<Sort> sorts;
	
	public List<Sort> getSorts()
	{
		return sorts;
	}
	public void setSorts(List<Sort> sorts)
	{
		this.sorts = sorts;
	}
	public Integer getPageNow()
	{
		return pageNow;
	}
	public void setPageNow(Integer pageNow)
	{
		this.pageNow = pageNow;
	}
	public Integer getPageSize()
	{
		return pageSize;
	}
	public void setPageSize(Integer pageSize)
	{
		this.pageSize = pageSize;
	}
	public Long getRowSize()
	{
		return rowSize;
	}
	public void setRowSize(Long rowSize)
	{
		Integer totle = 0;
		if(rowSize % pageSize == 0){
			totle = (int) (rowSize / pageSize);
		}else{
			totle = (int) (rowSize / pageSize) + 1; 
		}
		this.rowSize = rowSize;
		setTotle(totle);
	}
	public Integer getTotle()
	{
		return totle;
	}
	public void setTotle(Integer totle)
	{
		this.totle = totle;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Page [pageNow=" + pageNow + ", pageSize=" + pageSize
				+ ", rowSize=" + rowSize + ", totle=" + totle + ", sorts="
				+ sorts + "]";
	}
	
	
	
	
}
