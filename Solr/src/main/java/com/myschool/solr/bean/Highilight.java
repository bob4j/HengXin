/*
 * @(#)Highilight.java 2014-12-19 下午3:32:20
 * Copyright 2014 鲍建明, Inc. All rights reserved. 8637.com
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myschool.solr.bean;

import java.util.List;

/**
 * <p>File：Highilight.java</p>
 * <p>Title: </p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2014 2014-12-19 下午3:32:20</p>
 * <p>Company: 8637.com</p>
 * @author 鲍建明
 * @version 1.0
 */
public class Highilight
{
	/**
	 * 默认不开启高亮
	 */
	private Boolean isHighilight = false;				
	
	/**
	 * 需要高亮的字段
	 */
	private List<String> fields;
	
	/**
	 * 高亮字段的前缀
	 */
	private String Prefix = "<font color='red'>";
	
	/**
	 * 高亮字段的后缀
	 */
	private String Suffix = "</font>";
	
	/**
	 * 结果分片数，默认为1
	 */
	private Integer Snippets = 2;
	
	/**
	 * 每个分片的最大长度，默认为100
	 */
	private Integer Fragsize = 100;

	public Boolean getIsHighilight()
	{
		return isHighilight;
	}

	public void setIsHighilight(Boolean isHighilight)
	{
		this.isHighilight = isHighilight;
	}

	public List<String> getFields()
	{
		return fields;
	}

	public void setFields(List<String> fields)
	{
		this.fields = fields;
	}

	public String getPrefix()
	{
		return Prefix;
	}

	public void setPrefix(String prefix)
	{
		Prefix = prefix;
	}

	public String getSuffix()
	{
		return Suffix;
	}

	public void setSuffix(String suffix)
	{
		Suffix = suffix;
	}

	public Integer getSnippets()
	{
		return Snippets;
	}

	public void setSnippets(Integer snippets)
	{
		Snippets = snippets;
	}

	public Integer getFragsize()
	{
		return Fragsize;
	}

	public void setFragsize(Integer fragsize)
	{
		Fragsize = fragsize;
	}
	
}
