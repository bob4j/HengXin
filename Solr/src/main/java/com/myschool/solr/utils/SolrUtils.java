/*
 * @(#)SolrUtils.java 2014-12-19 上午11:49:02
 * Copyright 2014 鲍建明, Inc. All rights reserved. 8637.com
 * PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.myschool.solr.utils;

import java.io.IOException;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>File：SolrUtils.java</p>
 * <p>Title: </p>
 * <p>Description:</p>
 * <p>Copyright: Copyright (c) 2014 2014-12-19 上午11:49:02</p>
 * <p>Company: 8637.com</p>
 * @author 鲍建明
 * @version 1.0
 */
public class SolrUtils
{

	private static Logger logger = LoggerFactory.getLogger(SolrUtils.class);
	
	public static void commit(SolrServer server){
		try
		{
			if(server != null){
				server.commit();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error("Solr服务已经关闭", e);
			rollback(server);
		}
	}
	
	public static void rollback(SolrServer server){
		try
		{
			if(server != null){
				server.rollback();
			}
		}
		catch (SolrServerException e)
		{
			e.printStackTrace();
			logger.error("Solr服务已经关闭", e);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			logger.error("Solr服务IO异常", e);
		}
	}
}
