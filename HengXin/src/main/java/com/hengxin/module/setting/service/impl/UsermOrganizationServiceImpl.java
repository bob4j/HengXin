/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.service.impl</p>
 * <p>文件名：UsermOrganizationServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-21-上午10:58:42</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.hengxin.module.setting.entity.UsermOrganization;
import com.hengxin.module.setting.repository.UsermOrganizationRepository;
import com.hengxin.module.setting.service.UsermOrganizationService;

/**<p>名称：UsermOrganizationServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-21 上午10:58:42
 * @version 1.0.0
 */
@Service
public class UsermOrganizationServiceImpl extends BaseServiceImpl<UsermOrganization, Long> implements UsermOrganizationService{

	
	@Autowired
	private UsermOrganizationRepository usermOrganizationRepository;
	
	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.UsermOrganizationService#findOrganIdByUsermId(java.lang.Long)
	 */
	@Override
	public List<Long> findOrganIdByUsermId(Long usermId) {
		return usermOrganizationRepository.findOrganIdByUsermId(usermId);
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.UsermOrganizationService#save(java.lang.Long, java.lang.Long)
	 */
	@Override
	public UsermOrganization save(Long usermId, Long organId) {
		UsermOrganization uo = new UsermOrganization();
		uo.setOrganizationId(organId);
		uo.setUserId(usermId);
		return super.save(uo);
	}

}
