package com.hengxin.module.setting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
/**
 * Usermrole entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "usermrole")
public class UsermRole implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 7178018131176719596L;
	private Long id;
	private Long usermId;
	private Long roleId;

	// Constructors

	/** default constructor */
	public UsermRole() {
	}

	/** full constructor */
	public UsermRole(Long usermId, Long roleId) {
		this.usermId = usermId;
		this.roleId = roleId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "usermId", nullable = false)
	public Long getUsermId() {
		return this.usermId;
	}

	public void setUsermId(Long usermId) {
		this.usermId = usermId;
	}

	@Column(name = "roleId", nullable = false)
	public Long getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

}