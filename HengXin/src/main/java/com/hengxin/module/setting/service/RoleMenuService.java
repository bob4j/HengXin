/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.service</p>
 * <p>文件名：RoleMenuService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-20-下午4:13:32</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service;

import java.util.List;

import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.setting.entity.Role;
import com.hengxin.module.setting.entity.RoleMenu;

/**<p>名称：RoleMenuService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-20 下午4:13:32
 * @version 1.0.0
 */
public interface RoleMenuService extends BaseService<RoleMenu, Long>{

	
	public void save(Role role, List<Long> menuIds);
	
	
	/**
	 * 
	 * <p>描述：通过分身ID来查询对应的菜单ID集合</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param roleId
	 * @return
	 */
	public List<Long> findByRoleId(Long roleId);
	
	/**
	 * 
	 * <p>描述：通过身份ID来删除</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param roleId
	 */
	public void deleteByRoleId(Long roleId);
}
