/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.service</p>
 * <p>文件名：OrganizationService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-21-上午9:21:09</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service;

import java.util.List;

import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.setting.entity.Organization;
import com.hengxin.module.setting.model.ComboTree;

/**<p>名称：OrganizationService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-21 上午9:21:09
 * @version 1.0.0
 */
public interface OrganizationService extends BaseService<Organization, Long>{

	
	/**
	 * 
	 * <p>描述：通过当前的机构ID来查询对应机构的树结构</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param organizationId
	 * @return
	 */
	public List<Organization> findByOrganizationId(Long organizationId);
	
	/**
	 * 
	 * <p>描述：通过当前的机构ID来查询对应机构的树结构</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param organizationId
	 * @return
	 */
	public ComboTree getComboTree(Long organizationId);
	
	public Organization saveOrganization(Organization organization);
	
	public Organization updateOrganization(Organization organization)throws BusinessException;
	
}
