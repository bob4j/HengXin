/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service.impl</p>
 * <p>文件名：RoleServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-17-下午5:13:55</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.singno.bob.common.utils.CalendarUtil;
import cn.singno.bob.common.utils.EntityUtils;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.hengxin.constant.HengXinBusinessEnum;
import com.hengxin.module.setting.entity.Role;
import com.hengxin.module.setting.model.ComboTree;
import com.hengxin.module.setting.model.RoleModel;
import com.hengxin.module.setting.repository.RoleRepository;
import com.hengxin.module.setting.service.RoleMenuService;
import com.hengxin.module.setting.service.RoleService;

/**<p>名称：RoleServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-17 下午5:13:55
 * @version 1.0.0
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<Role, Long> implements RoleService{

	private static final Long DEFAULT_ORGANIZATION_ID = 1L; 
	
	@Autowired
	private RoleMenuService roleMenuService;
	
	@Autowired
	private RoleRepository roleRepository;
	
	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.RoleService#saveRole(com.hengxin.module.setting.entity.Role)
	 */
	@Override
	public Role saveRole(RoleModel roleModel) {
		Role role = new Role();
		EntityUtils.copyBean(role, roleModel);
		role.setCteateTime(CalendarUtil.getCurrentTime());
		role.setOrganizationId(DEFAULT_ORGANIZATION_ID);
		role.setId(null);
		role = super.save(role);
		roleMenuService.save(role, roleModel.getMenuIds());
		return role;
	}
	

	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.RoleService#updateRole(com.hengxin.module.setting.entity.Role)
	 */
	@Override
	public Role updateRole(RoleModel roleModel) throws BusinessException{
		Role old = super.findOne(roleModel.getId());
		if(old == null){
			throw new BusinessException(HengXinBusinessEnum.RECORD_NOT_FIND);
		}
		Role role = new Role();
		EntityUtils.copyBean(role, roleModel);
		role.setCteateTime(old.getCteateTime());
		role.setOrganizationId(old.getOrganizationId());
		role.setState(roleModel.getState());
		role = super.updata(role);
		roleMenuService.deleteByRoleId(role.getId());
		roleMenuService.save(role, roleModel.getMenuIds());
		return role;
	}


	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.RoleService#findById(java.lang.Long)
	 */
	@Override
	public RoleModel findById(Long id) {
		Role role = super.findOne(id);
		if(role == null){
			return null;
		}
		RoleModel roleModel = new RoleModel();
		EntityUtils.copyBean(roleModel, role);
		List<Long> menuIds = roleMenuService.findByRoleId(id);
		roleModel.setMenuIds(menuIds);
		return roleModel;
	}


	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.RoleService#getComboTree()
	 */
	@Override
	public List<ComboTree> getComboTree(Long organizationId) {
		return roleRepository.getComboTree(organizationId);
	}


	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.RoleService#findByIdentifier(java.lang.Long, java.lang.Long)
	 */
	@Override
	public Set<String> findByIdentifier(Long organizationId, Long userId) {
		return roleRepository.findByIdentifier(organizationId, userId);
	}


	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.RoleService#findRoleId(java.lang.Long, java.lang.Long)
	 */
	@Override
	public Set<Long> findRoleId(Long organizationId, Long userId) {
		return roleRepository.findRoleId(organizationId, userId);
	}

}
