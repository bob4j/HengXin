package com.hengxin.module.setting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Role entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "role")
public class Role implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -904580755038880322L;
	private Long id;
	private String name;
	private String identifier;
	private String functionMeta;
	private Short state;
	private Long cteateTime;
	private Long organizationId;

	// Constructors

	/** default constructor */
	public Role() {
	}

	/** minimal constructor */
	public Role(String name, Short state, Long organizationId) {
		this.name = name;
		this.state = state;
		this.organizationId = organizationId;
	}

	/** full constructor */
	public Role(String name, String functionMeta,  Short state,
			Long cteateTime, Long organizationId) {
		this.name = name;
		this.functionMeta = functionMeta;
		this.state = state;
		this.cteateTime = cteateTime;
		this.organizationId = organizationId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 60)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	

	/**
	 * @return the functionMeta
	 */
	@Column(name = "functionMeta", length = 120)
	public String getFunctionMeta() {
		return functionMeta;
	}

	/**
	 * @param functionMeta the functionMeta to set
	 */
	public void setFunctionMeta(String functionMeta) {
		this.functionMeta = functionMeta;
	}

	@Column(name = "state", nullable = false)
	public Short getState() {
		return this.state;
	}

	public void setState(Short state) {
		this.state = state;
	}

	@Column(name = "cteateTime")
	public Long getCteateTime() {
		return this.cteateTime;
	}

	public void setCteateTime(Long cteateTime) {
		this.cteateTime = cteateTime;
	}

	

	@Column(name = "organizationId", nullable = false)
	public Long getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * @return the identifier
	 */
	@Column(name = "identifier", nullable = false, length = 30)
	public String getIdentifier() {
		return identifier;
	}

	/**
	 * @param identifier the identifier to set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
	
	
	

}