/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.service.impl</p>
 * <p>文件名：OrganizationServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-21-上午9:21:26</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.singno.bob.common.utils.CalendarUtil;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.google.common.collect.Lists;
import com.hengxin.constant.HengXinBusinessEnum;
import com.hengxin.constant.StateConstant;
import com.hengxin.module.setting.entity.Organization;
import com.hengxin.module.setting.model.ComboTree;
import com.hengxin.module.setting.repository.OrganizationRepository;
import com.hengxin.module.setting.service.OrganizationService;

/**<p>名称：OrganizationServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-21 上午9:21:26
 * @version 1.0.0
 */
@Service
public class OrganizationServiceImpl extends BaseServiceImpl<Organization, Long> implements OrganizationService{

	@Autowired
	private OrganizationRepository organizationRepository;
	
	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.OrganizationService#findByOrganizationId(java.lang.Long)
	 */
	@Override
	public List<Organization> findByOrganizationId(Long organizationId) {
		Organization o = organizationRepository.findOne(organizationId);
		if(o != null){
			List<Organization> oo = organizationRepository.findByRootId(o.getRootId());
			return oo;
		}
		return Lists.newArrayList();
	}
	

	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.OrganizationService#getComboTree(java.util.List)
	 */
	@Override
	public ComboTree getComboTree(Long organizationId) {
		ComboTree root = organizationRepository.findComboTreeRoot(organizationId);
		findChild(root);
		return root;
	}
	
	//查询子集
	private void findChild(ComboTree root){
		if(root != null){
			List<ComboTree> trees = organizationRepository.findChild(root.getId());
			root.setChildren(trees);
			for (ComboTree tree : trees) {
				findChild(tree);
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.OrganizationService#saveOrganization(com.hengxin.module.setting.entity.Organization)
	 */
	@Override
	public Organization saveOrganization(Organization organization) {
		ComboTree c = organizationRepository.findComboTreeRoot(organization.getParentId());
		organization.setCreateTime(CalendarUtil.getCurrentTime());
		organization.setState(StateConstant.STATE_0);
		if(c != null){
			organization.setRootId(c.getId());
			organization = super.save(organization);
		}else{								//根目录平级
			organization = super.save(organization);
			organization.setRootId(organization.getId());
			super.baseRepository.flush();
		}
		return organization;
	}


	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.OrganizationService#updateOrganization(com.hengxin.module.setting.entity.Organization)
	 */
	@Override
	public Organization updateOrganization(Organization organization) throws BusinessException {
		
		Organization old = super.findOne(organization.getId());
		if(old == null){
			throw new BusinessException(HengXinBusinessEnum.RECORD_NOT_FIND);
		}
		if(StringUtils.isBlank(organization.getLogo())){
			organization.setLogo(old.getLogo());
		}
		organization.setCreateTime(old.getCreateTime());
		
		ComboTree c = organizationRepository.findComboTreeRoot(organization.getParentId());
		
		if(c != null){
			organization.setRootId(c.getId());
			organization = super.updata(organization);
		}else{								//根目录平级
			organization = super.updata(organization);
			organization.setRootId(organization.getId());
			super.baseRepository.flush();
		}
		return organization;
	}
	
	
	
	

}
