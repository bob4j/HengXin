/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.service</p>
 * <p>文件名：UsermRoleService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-20-下午8:13:31</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service;

import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.setting.entity.UsermRole;

/**<p>名称：UsermRoleService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-20 下午8:13:31
 * @version 1.0.0
 */
public interface UsermRoleService extends BaseService<UsermRole, Long>{

	public void save(Long usermId, Long roleId);
	
}
