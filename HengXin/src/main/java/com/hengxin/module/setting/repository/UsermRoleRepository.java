/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.repository</p>
 * <p>文件名：UsermRoleRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-20-下午8:12:56</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.repository;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.setting.entity.UsermRole;

/**<p>名称：UsermRoleRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-20 下午8:12:56
 * @version 1.0.0
 */
public interface UsermRoleRepository extends BaseRepository<UsermRole, Long>{

}
