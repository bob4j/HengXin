/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.service.impl</p>
 * <p>文件名：UsermRoleServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-20-下午8:13:49</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.google.common.collect.Lists;
import com.hengxin.module.setting.entity.UsermRole;
import com.hengxin.module.setting.service.UsermRoleService;

/**<p>名称：UsermRoleServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-20 下午8:13:49
 * @version 1.0.0
 */
@Service
public class UsermRoleServiceImpl extends BaseServiceImpl<UsermRole, Long> implements UsermRoleService{

	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.UsermRoleService#save(java.lang.Long, java.util.List)
	 */
	@Override
	public void save(Long usermId, Long roleId) {
		UsermRole usermRole = new UsermRole();
		usermRole.setUsermId(usermId);
		usermRole.setRoleId(roleId);
		super.save(usermRole);
	}

}
