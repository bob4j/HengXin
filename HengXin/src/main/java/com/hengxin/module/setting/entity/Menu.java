package com.hengxin.module.setting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Menu entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "menu")
public class Menu implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1197582017347873371L;
	private Long id;
	private String name;
	private String uri;
	private String logo;
	private String permission;
	private String mateDec;
	private Boolean delState;
	private Short orderField;
	@JSONField(name="_parentId")
	private Long parentId;

	// Constructors

	/** default constructor */
	public Menu() {
	}

	/** minimal constructor */
	public Menu(String name, String uri, Boolean delState, Short orderField) {
		this.name = name;
		this.uri = uri;
		this.delState = delState;
		this.orderField = orderField;
	}

	/** full constructor */
	public Menu(String name, String uri, String logo, String permission,
			String mateDec, Boolean delState, Short orderField, Long parentId) {
		this.name = name;
		this.uri = uri;
		this.logo = logo;
		this.permission = permission;
		this.mateDec = mateDec;
		this.delState = delState;
		this.orderField = orderField;
		this.parentId = parentId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 30)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "uri",  length = 120)
	public String getUri() {
		return this.uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Column(name = "logo", length = 120)
	public String getLogo() {
		return this.logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	@Column(name = "permission", length = 60)
	public String getPermission() {
		return this.permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	@Column(name = "mateDec", length = 200)
	public String getMateDec() {
		return this.mateDec;
	}

	public void setMateDec(String mateDec) {
		this.mateDec = mateDec;
	}

	@Column(name = "delState", nullable = false)
	public Boolean getDelState() {
		return this.delState;
	}

	public void setDelState(Boolean delState) {
		this.delState = delState;
	}

	@Column(name = "orderField")
	public Short getOrderField() {
		return this.orderField;
	}

	public void setOrderField(Short orderField) {
		this.orderField = orderField;
	}

	@Column(name = "parentId")
	public Long getParentId() {
		return this.parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

}