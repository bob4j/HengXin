/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.service.impl</p>
 * <p>文件名：RoleMenuServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-20-下午4:13:54</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.google.common.collect.Lists;
import com.hengxin.module.setting.entity.Role;
import com.hengxin.module.setting.entity.RoleMenu;
import com.hengxin.module.setting.repository.RoleMenuRepository;
import com.hengxin.module.setting.service.RoleMenuService;

/**<p>名称：RoleMenuServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-20 下午4:13:54
 * @version 1.0.0
 */
@Service
public class RoleMenuServiceImpl extends BaseServiceImpl<RoleMenu, Long> implements RoleMenuService{

	@Autowired
	private RoleMenuRepository roleMenuRepository;
	
	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.RoleMenuService#save(java.lang.Long, java.util.List)
	 */
	@Override
	public void save(Role role, List<Long> menuIds) {
		if(CollectionUtils.isNotEmpty(menuIds)){
			List<RoleMenu> roleMenus = Lists.newArrayList();
			for (Long menuId : menuIds) {
				RoleMenu roleMenu = new RoleMenu();
				roleMenu.setMenuId(menuId);
				roleMenu.setRoleId(role.getId());
				roleMenus.add(roleMenu);
			}
			roleMenuRepository.save(roleMenus);
		}
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.RoleMenuService#findByRoleId(java.lang.Long)
	 */
	@Override
	public List<Long> findByRoleId(Long roleId) {
		return roleMenuRepository.findByRoleId(roleId);
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.RoleMenuService#deleteByRoleId(java.lang.Long)
	 */
	@Override
	public void deleteByRoleId(Long roleId) {
		roleMenuRepository.deleteByRoleId(roleId);
		
	}

}
