/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.repository</p>
 * <p>文件名：RoleMenuRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-20-下午4:09:35</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.setting.entity.RoleMenu;

/**<p>名称：RoleMenuRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-20 下午4:09:35
 * @version 1.0.0
 */
public interface RoleMenuRepository extends BaseRepository<RoleMenu, Long>{

	@Query("select o.menuId from RoleMenu o where o.roleId = ?1")
	public List<Long> findByRoleId(Long roleId);
	
	
	/**
	 * 
	 * <p>描述：通过身份ID来删除</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param roleId
	 */
	@Query("delete from RoleMenu o where o.roleId = ?1")
	@Modifying
	public int deleteByRoleId(Long roleId);
}
