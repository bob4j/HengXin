/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service.impl</p>
 * <p>文件名：MenuServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-17-下午5:15:43</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service.impl;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.google.common.collect.Sets;
import com.hengxin.constant.HengXinBusinessEnum;
import com.hengxin.module.setting.entity.Menu;
import com.hengxin.module.setting.model.ComboTree;
import com.hengxin.module.setting.repository.MenuRepository;
import com.hengxin.module.setting.service.MenuService;
import com.hengxin.module.setting.service.RoleService;

/**<p>名称：MenuServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-17 下午5:15:43
 * @version 1.0.0
 */
@Service
public class MenuServiceImpl extends BaseServiceImpl<Menu, Long> implements MenuService{

	@Autowired
	private MenuRepository menuRepository;
	
	@Autowired
	private RoleService roleService;
	
	
	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.MenuService#getComboTree()
	 */
	@Override
	public List<ComboTree> getComboTree() {
		List<ComboTree> tree = menuRepository.getComboTree();
		setTree(tree);
		return tree;
	}

	//递归获取子集菜单
	private void setTree(List<ComboTree> tree){
		if(CollectionUtils.isNotEmpty(tree)){
			for (ComboTree comboTree : tree) {
				List<ComboTree> ct = menuRepository.getComboTree(comboTree.getId());
				comboTree.setChildren(ct);
				setTree(ct);
			}
		}
	}


	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.MenuService#saveMenu(com.hengxin.module.setting.entity.Menu)
	 */
	@Override
	public Menu saveMenu(Menu menu) {
		menu.setDelState(false);
		return super.save(menu);
	}


	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.MenuService#updateMenu(com.hengxin.module.setting.entity.Menu)
	 */
	@Override
	public Menu updateMenu(Menu menu) throws BusinessException {
		Menu old = super.findOne(menu.getId());
		if(old == null ){
			throw new BusinessException(HengXinBusinessEnum.RECORD_NOT_FIND);
		}
		if(StringUtils.isBlank(menu.getLogo())){
			menu.setLogo(old.getLogo());
		}
		menu.setDelState(old.getDelState());
		return super.updata(menu);
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.setting.service.MenuService#findMenu(java.lang.Long, java.lang.Long)
	 */
	@Override
	public Set<String> findMenuPermission(Long organizationId, Long userId) {
		Set<Long> roleIds = roleService.findRoleId(organizationId, userId);
		Set<String> menus = Sets.newHashSet();
		if(CollectionUtils.isNotEmpty(roleIds)){
			for (Long roleId : roleIds) {
				Set<String> menu = menuRepository.findMenuPermission(roleId);
				menus.addAll(menu);
			}
		}
		return menus;
	}
}
