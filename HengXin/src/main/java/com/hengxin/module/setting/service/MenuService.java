/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service</p>
 * <p>文件名：MenuService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-17-下午5:15:19</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service;

import java.util.List;
import java.util.Set;

import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.setting.entity.Menu;
import com.hengxin.module.setting.model.ComboTree;

/**<p>名称：MenuService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-17 下午5:15:19
 * @version 1.0.0
 */
public interface MenuService extends BaseService<Menu, Long>{

	
	public List<ComboTree> getComboTree();

	public Menu saveMenu(Menu menu);

	public Menu updateMenu(Menu menu) throws BusinessException;
	
	/**
	 * 
	 * <p>描述：通过用户ID和组织ID来查询对应的身份信息</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param organizationId
	 * @param userId
	 * @return
	 */
	public Set<String> findMenuPermission(Long organizationId, Long userId);
}
