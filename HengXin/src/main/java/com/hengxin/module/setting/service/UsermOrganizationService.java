/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.service</p>
 * <p>文件名：UsermOrganizationService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-21-上午10:58:25</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service;

import java.util.List;

import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.setting.entity.UsermOrganization;

/**<p>名称：UsermOrganizationService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-21 上午10:58:25
 * @version 1.0.0
 */
public interface UsermOrganizationService extends BaseService<UsermOrganization, Long>{

	
	/**
	 * 
	 * <p>描述：通过用户ID来查询对应的机构ID</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param usermId
	 * @return
	 */
	public List<Long> findOrganIdByUsermId(Long usermId);
	
	public UsermOrganization save(Long usermId, Long organId);
}
