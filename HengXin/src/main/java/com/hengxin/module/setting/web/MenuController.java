/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.web</p>
 * <p>文件名：MenuController.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-20-下午1:14:44</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.singno.bob.bean.JsonMessage;
import cn.singno.bob.emun.CommonEmun;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.utils.JsonMessageUtils;
import cn.singno.bob.web.base.BaseController;
import cn.singno.bob.web.bean.FileResult;
import cn.singno.bob.web.bean.Multipart;
import cn.singno.bob.web.utils.MultipartUtils;

import com.alibaba.fastjson.JSON;
import com.hengxin.constant.ApplicationContext;
import com.hengxin.module.setting.entity.Menu;
import com.hengxin.module.setting.model.ComboTree;
import com.hengxin.module.setting.service.MenuService;
import com.hengxin.utils.DeleteModelUtils;

/**<p>名称：MenuController.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-20 下午1:14:44
 * @version 1.0.0
 */
@Controller
@RequestMapping(ApplicationContext.SETTING + "/menu")
public class MenuController extends BaseController {

	
	@Autowired
	private MenuService menuService;
	
	@RequestMapping("/list")
	public String list(){
		return "menu/list";
	}
	
	/**
	 * 
	 * <p>描述：获取下拉列表树</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	@RequestMapping("/combotree")
	@ResponseBody
	public String ComboTree(){
		List<ComboTree> trees = menuService.getComboTree();
		return JSON.toJSONString(trees);
	}
	
	
	@RequestMapping("/list/data")
	@ResponseBody
	public JsonMessage listData(Menu searchBean,
			@PageableDefault Pageable page){
		Page<Menu> result = menuService.findPageList(searchBean,
				page);
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS, result);
	}
	
	@RequestMapping("/load")
	public String load(Long id, Model model){
		Menu menu = menuService.findOne(id);
		if(menu != null){
			model.addAttribute("menu", menu);
		}
		return "menu/add";
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST )
	public JsonMessage saveOrUpdate(Menu menu, MultipartFile file, HttpServletRequest request) throws BusinessException {
		if(file != null && !file.isEmpty()){
			FileResult result = MultipartUtils.uploadImage(new Multipart(file));
			menu.setLogo(result.getUri());
		}
		if(menu.getId() == null){
			menuService.saveMenu(menu);
		}else{
			menuService.updateMenu(menu);
		}
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/deleteAll" , method = RequestMethod.POST )
	public JsonMessage deleteAll(@RequestBody  List<Object> menus) throws BusinessException {
		menuService.delete(DeleteModelUtils.toDeleteModel(menus, Menu.class));
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	
}
