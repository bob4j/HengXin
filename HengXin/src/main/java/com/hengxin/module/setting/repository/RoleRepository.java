/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.repository</p>
 * <p>文件名：RoleRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-17-下午5:13:04</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.setting.entity.Role;
import com.hengxin.module.setting.model.ComboTree;

/**<p>名称：RoleRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-17 下午5:13:04
 * @version 1.0.0
 */
public interface RoleRepository extends BaseRepository<Role, Long>{

	@Query("select new ComboTree(o.id, o.name) from Role o where o.state = 0 and o.organizationId = ?1")
	public List<ComboTree> getComboTree(Long organizationId);
	
	

	/**
	 * 
	 * <p>描述：通过用户ID和组织ID来查询对应的身份信息</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param organizationId
	 * @param userId
	 * @return
	 */
	@Query("select o.identifier from Role o, UsermRole u where u.usermId = ?2 and o.organizationId = ?1 and o.id = u.roleId")
	public Set<String> findByIdentifier(Long organizationId, Long userId);
	
	
	
	/**
	 * 
	 * <p>描述：通过用户ID和组织ID来查询对应的身份信息</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param organizationId
	 * @param userId
	 * @return
	 */
	@Query("select o.id from Role o, UsermRole u where u.usermId = ?2 and o.organizationId = ?1 and o.id = u.roleId")
	public Set<Long> findRoleId(Long organizationId, Long userId);
	
}
