/**<p>项目名：</p>
 * <p>包名：	com.hengxin.bean</p>
 * <p>文件名：TreeModel.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-18-下午4:57:06</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**<p>名称：TreeModel.java</p>
 * <p>描述：easyui 中 ComboTree 对象</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-18 下午4:57:06
 * @version 1.0.0
 */
@Entity
public class ComboTree implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1863405897309303117L;

	@Id
	private Long id;
	
	private String text;
	
	@Transient
	private List<ComboTree> children;
	

	/**
	 * <p>构造器：</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param id
	 * @param text
	 */
	public ComboTree(Long id, String text) {
		super();
		this.id = id;
		this.text = text;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the children
	 */
	@Transient
	public List<ComboTree> getChildren() {
		return children;
	}

	/**
	 * @param children the children to set
	 */
	public void setChildren(List<ComboTree> children) {
		this.children = children;
	}
	
	
	
	
}
