/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.repository</p>
 * <p>文件名：MenuRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-17-下午5:14:32</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.setting.entity.Menu;
import com.hengxin.module.setting.model.ComboTree;

/**<p>名称：MenuRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-17 下午5:14:32
 * @version 1.0.0
 */
public interface MenuRepository extends BaseRepository<Menu, Long>{

	
	@Query("select new ComboTree(o.id, o.name) from Menu o where o.parentId = ?1 and o.delState = 0")
	public List<ComboTree> getComboTree(Long parentId) ;
	
	
	@Query("select new ComboTree(o.id, o.name) from Menu o where o.delState = 0 and o.parentId is null")
	public List<ComboTree> getComboTree() ;
	
	
	
	@Query("select o.id from Menu o, RoleMenu r where r.roleId = ?1 and o.id = r.menuId")
	public Set<String> findMenuPermission(Long roleId);
}
