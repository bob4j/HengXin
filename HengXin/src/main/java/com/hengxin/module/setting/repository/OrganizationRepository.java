/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.repository</p>
 * <p>文件名：OrganizationRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-21-上午9:20:48</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.setting.entity.Organization;
import com.hengxin.module.setting.model.ComboTree;

/**<p>名称：OrganizationRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-21 上午9:20:48
 * @version 1.0.0
 */
public interface OrganizationRepository extends BaseRepository<Organization, Long>{

	@Query("select o from Organization o where o.parentId = ?1")
	public List<Organization> findByParentId(Long parentId);
	
	/**
	 * 
	 * <p>描述：获取当前组织的父级</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	@Query("select new ComboTree(o.id, o.name) from Organization o,Organization a where   a.id = ?1 and o.id = a.rootId")
	public ComboTree findComboTreeRoot(Long cureentId);
	
	@Query("select o from Organization o where o.rootId = ?1")
	public List<Organization> findByRootId(Long rootId);
	
	@Query("select new ComboTree(o.id, o.name) from Organization o where o.parentId = ?1 ")
	public List<ComboTree> findChild(Long parentId);
	
}
