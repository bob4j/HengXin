/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.model</p>
 * <p>文件名：RoleModel.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-20-下午4:01:55</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.model;

import java.io.Serializable;
import java.util.List;

/**<p>名称：RoleModel.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-20 下午4:01:55
 * @version 1.0.0
 */
public class RoleModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3438249289972019481L;
	private Long id;
	private String name;
	private String functionMeta;
	private Short state;
	private Long organizationId;
	private List<Long> menuIds;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the functionMeta
	 */
	public String getFunctionMeta() {
		return functionMeta;
	}
	/**
	 * @param functionMeta the functionMeta to set
	 */
	public void setFunctionMeta(String functionMeta) {
		this.functionMeta = functionMeta;
	}
	/**
	 * @return the state
	 */
	public Short getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(Short state) {
		this.state = state;
	}
	/**
	 * @return the organizationId
	 */
	public Long getOrganizationId() {
		return organizationId;
	}
	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}
	/**
	 * @return the menuIds
	 */
	public List<Long> getMenuIds() {
		return menuIds;
	}
	/**
	 * @param menuIds the menuIds to set
	 */
	public void setMenuIds(List<Long> menuIds) {
		this.menuIds = menuIds;
	}
	
	
	
}
