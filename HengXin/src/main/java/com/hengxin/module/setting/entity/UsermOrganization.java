package com.hengxin.module.setting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Usermorganization entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "usermorganization")
public class UsermOrganization implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 2207719226937163780L;
	private Long id;
	private Long userId;
	private Long organizationId;

	// Constructors

	/** default constructor */
	public UsermOrganization() {
	}

	/** full constructor */
	public UsermOrganization(Long userId, Long organizationId) {
		this.userId = userId;
		this.organizationId = organizationId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "userId", nullable = false)
	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "organizationId", nullable = false)
	public Long getOrganizationId() {
		return this.organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

}