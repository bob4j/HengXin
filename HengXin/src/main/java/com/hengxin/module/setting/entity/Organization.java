package com.hengxin.module.setting.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import cn.singno.bob.common.utils.CalendarUtil;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Organization entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "organization")
public class Organization implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2683848430169398858L;
	// Fields

	private Long id;
	private String name;
	private String logo;
	private String honor;
	private String createUser;
	private String scale;
	private String mobile;
	private String mainBusiness;
	
	@DateTimeFormat(pattern = CalendarUtil.FMT_Y_M_D)
	private Long ctime;
	private Long createTime;
	private Short state;
	private Long parentId;
	private Long rootId;

	// Constructors

	/** default constructor */
	public Organization() {
	}

	/** minimal constructor */
	public Organization(String name, Long createTime, Short state) {
		this.name = name;
		this.createTime = createTime;
		this.state = state;
	}

	/** full constructor */
	public Organization(String name, String logo, String honor,
			String createUser, String scale, String mobile,
			String mainBusiness, Long ctime, Long createTime, Short state,
			Long parentId) {
		this.name = name;
		this.logo = logo;
		this.honor = honor;
		this.createUser = createUser;
		this.scale = scale;
		this.mobile = mobile;
		this.mainBusiness = mainBusiness;
		this.ctime = ctime;
		this.createTime = createTime;
		this.state = state;
		this.parentId = parentId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 80)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "logo", length = 120)
	public String getLogo() {
		return this.logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	@Column(name = "Honor", length = 120)
	public String getHonor() {
		return this.honor;
	}

	public void setHonor(String honor) {
		this.honor = honor;
	}

	@Column(name = "createUser", length = 60)
	public String getCreateUser() {
		return this.createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	@Column(name = "scale", length = 60)
	public String getScale() {
		return this.scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	@Column(name = "mobile", length = 30)
	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "mainBusiness", length = 120)
	public String getMainBusiness() {
		return this.mainBusiness;
	}

	public void setMainBusiness(String mainBusiness) {
		this.mainBusiness = mainBusiness;
	}

	@Column(name = "cTime")
	public Long getCtime() {
		return this.ctime;
	}

	public void setCtime(Long ctime) {
		this.ctime = ctime;
	}

	@Column(name = "createTime", nullable = false)
	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	@Column(name = "state", nullable = false)
	public Short getState() {
		return this.state;
	}

	public void setState(Short state) {
		this.state = state;
	}

	@Column(name = "parentId")
	@JSONField(name="_parentId")
	public Long getParentId() {
		return this.parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	/**
	 * @return the rootId
	 */
	@Column(name = "rootId")
	public Long getRootId() {
		return rootId;
	}

	/**
	 * @param rootId the rootId to set
	 */
	public void setRootId(Long rootId) {
		this.rootId = rootId;
	}
	
	

}