/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.repository</p>
 * <p>文件名：UsermOrganizationRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-21-上午10:58:05</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.setting.entity.UsermOrganization;

/**<p>名称：UsermOrganizationRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-21 上午10:58:05
 * @version 1.0.0
 */
public interface UsermOrganizationRepository extends BaseRepository<UsermOrganization, Long>{

	@Query("select o.organizationId from UsermOrganization o where o.userId = ?1")
	public List<Long> findOrganIdByUsermId(Long usermId) ;
	
}
