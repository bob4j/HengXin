/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service</p>
 * <p>文件名：RoleService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-17-下午5:13:29</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.service;

import java.util.List;
import java.util.Set;

import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.setting.entity.Role;
import com.hengxin.module.setting.model.ComboTree;
import com.hengxin.module.setting.model.RoleModel;

/**<p>名称：RoleService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-17 下午5:13:29
 * @version 1.0.0
 */
public interface RoleService extends BaseService<Role, Long>{

	
	public Role saveRole(RoleModel roleModel);
	
	
	public Role updateRole(RoleModel roleModel) throws BusinessException;
	
	/**
	 * 
	 * <p>描述：通过身份ID来查询，包含菜单对应的ID集合</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param id
	 * @return
	 */
	public RoleModel findById(Long id);
	
	
	/**
	 * 
	 * <p>描述：获取身份树</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	public List<ComboTree> getComboTree(Long organizationId);
	
	/**
	 * 
	 * <p>描述：通过用户ID和组织ID来查询对应的身份信息</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param organizationId
	 * @param userId
	 * @return
	 */
	public Set<String> findByIdentifier(Long organizationId, Long userId);
	
	/**
	 * 
	 * <p>描述：通过用户ID和组织ID来查询对应的身份信息</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param organizationId
	 * @param userId
	 * @return
	 */
	public Set<Long> findRoleId(Long organizationId, Long userId);
	
	
}
