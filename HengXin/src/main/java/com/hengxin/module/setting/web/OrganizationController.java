/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.web</p>
 * <p>文件名：OrganizationController.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-21-上午9:25:11</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.singno.bob.bean.JsonMessage;
import cn.singno.bob.emun.CommonEmun;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.utils.JsonMessageUtils;
import cn.singno.bob.web.base.BaseController;
import cn.singno.bob.web.bean.FileResult;
import cn.singno.bob.web.bean.Multipart;
import cn.singno.bob.web.utils.MultipartUtils;

import com.alibaba.fastjson.JSON;
import com.hengxin.bean.ShiroUserm;
import com.hengxin.constant.ApplicationContext;
import com.hengxin.module.common.service.UsermService;
import com.hengxin.module.setting.entity.Organization;
import com.hengxin.module.setting.model.ComboTree;
import com.hengxin.module.setting.service.OrganizationService;

/**<p>名称：OrganizationController.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-21 上午9:25:11
 * @version 1.0.0
 */
@Controller
@RequestMapping(ApplicationContext.SETTING + "/organization")
public class OrganizationController extends BaseController{

	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private UsermService usermService;
	
	@RequestMapping("/list")
	public String list(){
		return "organization/list";
	}
	
	
	@RequestMapping("/list/data")
	@ResponseBody
	public JsonMessage listData(){
		ShiroUserm shiroUserm = usermService.getCurrentUserm();
		List<Organization> result = organizationService.findByOrganizationId(shiroUserm.getOrganId());
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS, result);
	}
	
	@RequestMapping("/load")
	public String load(Long id, Model model){
		ShiroUserm shiroUserm = usermService.getCurrentUserm();
		ComboTree trees = organizationService.getComboTree(shiroUserm.getOrganId());
		Organization organization = organizationService.findOne(id);
		if(organization != null){
			model.addAttribute("organization", organization);
		}
		model.addAttribute("trees", JSON.toJSONString(trees));
		return "organization/add";
	}
	
	@RequestMapping(value = "/saveOrUpdate", method=RequestMethod.POST)
	@ResponseBody
	public JsonMessage saveOrUpdate(Organization organization, MultipartFile file, HttpServletRequest request) throws BusinessException{
		if(file != null && !file.isEmpty()){
			FileResult fileResult = MultipartUtils.uploadImage(new Multipart(file));
			organization.setLogo(fileResult.getUri());
		}
		if(organization.getId() == null){
			organizationService.saveOrganization(organization);
		}else{
			organizationService.updateOrganization(organization);
		}
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	
	
}
