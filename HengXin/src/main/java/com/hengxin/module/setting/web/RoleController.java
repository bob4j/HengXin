/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.setting.web</p>
 * <p>文件名：RoleController.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-20-上午9:39:59</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.setting.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.singno.bob.bean.JsonMessage;
import cn.singno.bob.emun.CommonEmun;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.utils.JsonMessageUtils;
import cn.singno.bob.web.base.BaseController;

import com.alibaba.fastjson.JSON;
import com.hengxin.constant.ApplicationContext;
import com.hengxin.module.setting.entity.Role;
import com.hengxin.module.setting.model.ComboTree;
import com.hengxin.module.setting.model.RoleModel;
import com.hengxin.module.setting.service.RoleService;
import com.hengxin.utils.DeleteModelUtils;

/**<p>名称：RoleController.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-20 上午9:39:59
 * @version 1.0.0
 */
@Controller
@RequestMapping(ApplicationContext.SETTING + "/role")
public class RoleController extends BaseController{

	@Autowired
	private RoleService roleService;
	
	@RequestMapping("/list")
	public String list(){
		return "role/list";
	}
	
	@RequestMapping("/list/data")
	@ResponseBody
	public JsonMessage listData(Role searchBean,
			@PageableDefault Pageable page){
		Page<Role> result = roleService.findPageList(searchBean,
				page);
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS, result);
	}
	
	@RequestMapping("/load")
	public String load(Long id, Model model){
		RoleModel role = roleService.findById(id);
		if(role != null){
			model.addAttribute("role", role);
		}
		return "role/add";
	}
	
	@ResponseBody
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST )
	public JsonMessage saveOrUpdate(@RequestBody RoleModel roleModel) throws BusinessException {
		if(roleModel.getId() == null){
			roleService.saveRole(roleModel);
		}else{
			roleService.updateRole(roleModel);
		}
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/deleteAll" , method = RequestMethod.POST )
	public JsonMessage deleteAll(@RequestBody  List<Object> roles) throws BusinessException {
		roleService.delete(DeleteModelUtils.toDeleteModel(roles, Role.class));
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	/**
	 * 
	 * <p>描述：获取下拉列表树</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	@RequestMapping("/combotree")
	@ResponseBody
	public String ComboTree(Long organizationId){
		List<ComboTree> trees = roleService.getComboTree(organizationId);
		return JSON.toJSONString(trees);
	}
	
	
}
