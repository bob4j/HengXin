/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.model</p>
 * <p>文件名：ProductModel.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-5-下午4:42:48</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.model;

import javax.persistence.Transient;

/**<p>名称：ProductModel.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-5 下午4:42:48
 * @version 1.0.0
 */
public abstract class ProductModel {

	@Transient
	private Integer stockNum = 1;

	/**
	 * @return the stockNum
	 */
	@Transient
	public Integer getStockNum() {
		return stockNum;
	}

	/**
	 * @param stockNum the stockNum to set
	 */
	public void setStockNum(Integer stockNum) {
		this.stockNum = stockNum;
	}
	
	
	
}
