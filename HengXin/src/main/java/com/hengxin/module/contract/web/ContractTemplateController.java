/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.web</p>
 * <p>文件名：ContractTemplateController.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-14-上午10:52:47</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import cn.singno.bob.bean.JsonMessage;
import cn.singno.bob.emun.CommonEmun;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.utils.JsonMessageUtils;
import cn.singno.bob.web.base.BaseController;
import cn.singno.bob.web.bean.FileResult;
import cn.singno.bob.web.bean.Multipart;
import cn.singno.bob.web.utils.MultipartUtils;

import com.hengxin.constant.ApplicationContext;
import com.hengxin.module.contract.entity.ContractTemplate;
import com.hengxin.module.contract.service.ContractTemplateService;
import com.hengxin.utils.DeleteModelUtils;

/**<p>名称：ContractTemplateController.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-14 上午10:52:47
 * @version 1.0.0
 */
@Controller
@RequestMapping(ApplicationContext.CONTRACT_MODULE + "/template")
public class ContractTemplateController extends BaseController{

	
	@Autowired
	private ContractTemplateService contractTemplateService;
	
	/**
	 * 
	 * <p>描述：合同模板列表页 </p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	@RequestMapping("/list")
	public String list(){
		return "contract/template_list";
	}
	
	/**
	 * 
	 * <p>描述：ajax数据</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "/list/data" )
	public JsonMessage listData(ContractTemplate searchBean, @PageableDefault Pageable page){
		Page<ContractTemplate> result = contractTemplateService.findPageList(searchBean, page);
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS, result);
	}
	
	
	/**
	 * 
	 * <p>描述：跳转新增和修改页面</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	@RequestMapping("/load")
	public String load(Long id, Model model){
		ContractTemplate template = contractTemplateService.findOne(id);
		if(template != null){
			model.addAttribute("template", template);
		}
		return "contract/template_add";
	}
	
	/**
	 * 
	 * <p>描述：模板的保存以及修改</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 * @throws BusinessException 
	 */
	@RequestMapping(value = "/saveOrUpdate", method=RequestMethod.POST)
	@ResponseBody
	public JsonMessage saveOrUpdate(MultipartFile file, ContractTemplate template, HttpServletRequest request) throws BusinessException{
		if(file == null || file.isEmpty()){
			contractTemplateService.saveOrUpdate(template, null, null);
		}else{
			FileResult result = MultipartUtils.uploadTxt(new Multipart(file));
			contractTemplateService.saveOrUpdate(template, result.getUri(), result.getSuffixName());
		}
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	
	
	/**
	 * 
	 * <p>描述：删除</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param contractTemplates
	 * @return
	 * @throws BusinessException
	 */
	@ResponseBody
	@RequestMapping(value = "/deleteAll" , method = RequestMethod.POST )
	public JsonMessage deleteAll(@RequestBody  List<Object> contractTemplates) throws BusinessException {
		contractTemplateService.delete(DeleteModelUtils.toDeleteModel(contractTemplates, ContractTemplate.class));
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	
}
