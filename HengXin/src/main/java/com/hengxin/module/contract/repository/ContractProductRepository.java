/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.repository</p>
 * <p>文件名：ContractProductRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:40:40</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.repository;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.contract.entity.ContractProduct;

/**<p>名称：ContractProductRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:40:40
 * @version 1.0.0
 */
public interface ContractProductRepository extends BaseRepository<ContractProduct, Long>{

	
	/**
	 * 
	 * <p>描述：通过合同ID来查询对应所有的产品信息</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param contractId
	 * @return
	 */
	@Query(value = "select new Map(p.id as id, p.number as number, p.salePrice as salePrice, p.standard as standard, p.stock as stock, o.stockNum as stockNum, p.stockPrice as stockPrice, p.unit as unit, p.voltage as voltage)  from ContractProduct o ,  Product p where  o.productId = p.id and o.contractId = ?1 ")
	public List<Map<String, Object>> findByContractId(Long contractId) ;
	
	/**
	 * 
	 * <p>描述：通过合同ID来删除所有对应的产品</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param contractId
	 */
	@Modifying
	@Query("delete from ContractProduct o where o.contractId = ?1")
	public void deleteByContractId(Long contractId);
	
}
