/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.model</p>
 * <p>文件名：ContractModel.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:44:18</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.model;

import java.util.List;

import javax.persistence.Transient;

import com.hengxin.module.contract.entity.Product;

/**<p>名称：ContractModel.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:44:18
 * @version 1.0.0
 */
public abstract class ContractModel {

	@Transient
	private List<Product> product;

	@Transient
	private String signed;
	
	@Transient
	private String start;
	@Transient
	private String end;
	
	//signedDate  instanStart  instanEnd
	/**
	 * @return the product
	 */
	@Transient
	public List<Product> getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(List<Product> product) {
		this.product = product;
	}

	/**
	 * @return the signed
	 */
	@Transient
	public String getSigned() {
		return signed;
	}

	/**
	 * @param signed the signed to set
	 */
	public void setSigned(String signed) {
		this.signed = signed;
	}

	/**
	 * @return the start
	 */
	@Transient
	public String getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(String start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	@Transient
	public String getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(String end) {
		this.end = end;
	}
	
	
}
