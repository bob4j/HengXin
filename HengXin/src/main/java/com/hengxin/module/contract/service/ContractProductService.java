/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.service</p>
 * <p>文件名：ContractProductService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:40:59</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.contract.entity.ContractProduct;
import com.hengxin.module.contract.entity.Product;

/**<p>名称：ContractProductService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:40:59
 * @version 1.0.0
 */
public interface ContractProductService extends BaseService<ContractProduct, Long>{

	/**
	 * 
	 * <p>描述：保存合同和产品的中间表，</p>
	 * <pre>
	 *    返回产品统计的产品总价
	 * </pre>
	 * @param contractId
	 * @param productIds
	 * @return
	 */
	public BigDecimal save(Long contractId, List<Product> products) throws BusinessException;
	
	
	/**
	 * 
	 * <p>描述：通过合同ID来查询产品的基本信息</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param contractId
	 * @return
	 */
	public List<Map<String, Object>> findByContractId(Long contractId);
	
	/**
	 * 
	 * <p>描述：通过合同ID来删除所有对应的产品</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param contractId
	 */
	public void deleteByContractId(Long contractId);
	
	
}
