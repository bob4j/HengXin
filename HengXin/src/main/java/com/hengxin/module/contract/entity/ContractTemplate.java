package com.hengxin.module.contract.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 合同模板
 * Contracttemplate entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="contracttemplate")
public class ContractTemplate  implements java.io.Serializable {


    // Fields    

     /**
	 * 
	 */
	private static final long serialVersionUID = 2483786989404739818L;
	private Long id;
    private String templateName;
    private String uri;
    private String fileType;
    private Long createTime;
     
    private Boolean isDefualt;


    // Constructors

    /** default constructor */
    public ContractTemplate() {
    }

	/** minimal constructor */
    public ContractTemplate(Long createTime) {
        this.createTime = createTime;
    }
    
    /** full constructor */
    public ContractTemplate(String templateName, String uri, String fileType, Long createTime) {
        this.templateName = templateName;
        this.uri = uri;
        this.fileType = fileType;
        this.createTime = createTime;
    }

   
    // Property accessors
    @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="id", unique=true, nullable=false)

    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(name="templateName", length=60)

    public String getTemplateName() {
        return this.templateName;
    }
    
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }
    
    @Column(name="uri", length=130)

    public String getUri() {
        return this.uri;
    }
    
    public void setUri(String uri) {
        this.uri = uri;
    }
    
   

    
    @Column(name="createTime", nullable=false)

    public Long getCreateTime() {
        return this.createTime;
    }
    
    /**
	 * @return the fileType
	 */
    @Column(name="fileType", length=60)
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

	/**
	 * @return the isDefualt
	 */
	 @Column(name="isDefualt", nullable=false)
	public Boolean getIsDefualt() {
		return isDefualt;
	}

	/**
	 * @param isDefualt the isDefualt to set
	 */
	public void setIsDefualt(Boolean isDefualt) {
		this.isDefualt = isDefualt;
	}
   








}