package com.hengxin.module.contract.web;

import java.util.List;
import java.util.Map;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.singno.bob.bean.JsonMessage;
import cn.singno.bob.emun.CommonEmun;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.utils.JsonMessageUtils;
import cn.singno.bob.web.base.BaseController;

import com.alibaba.fastjson.JSON;
import com.hengxin.constant.ApplicationContext;
import com.hengxin.module.contract.entity.Contract;
import com.hengxin.module.contract.service.ContractProductService;
import com.hengxin.module.contract.service.ContractService;
import com.hengxin.utils.DeleteModelUtils;

/**
 * <p>名称：ContractController.java</p>
 * <p>描述：合同</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-3 下午1:43:48
 * @version 1.0.0
 */
@Controller
@RequestMapping(ApplicationContext.CONTRACT_MODULE)
public class ContractController extends BaseController {
	
	@Autowired
	private ContractService contractService;
	
	@Autowired
	private ContractProductService contractProductService;
	
	
	/**
	 * 
	 * <p>描述：新增和修改页面跳转</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("/load")
	public String load(Long id, Model model) {
		Contract contract = contractService.findById(id);
		if(contract != null){
			List<Map<String, Object>> products = contractProductService.findByContractId(contract.getId());
			model.addAttribute("contract", contract);
			model.addAttribute("products", JSON.toJSONString(products));
		}
		return "contract/add";
	}
	
	
	/**
	 * 
	 * <p>描述：保存以及修改合同</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param contract
	 * @return
	 * @throws BusinessException
	 */
	@RequestMapping(value = "/saveOrUpdate", method = RequestMethod.POST)
	@ResponseBody
	public JsonMessage saveOrUpdate(@RequestBody Contract contract) throws BusinessException{
		if(contract.getId() == null){
			contractService.saveContract(contract);
		}else{
			contractService.updateContract(contract);
		}
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	

	/**
	 * 
	 * <p>描述：列表页跳转</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	@RequiresAuthentication
	@RequiresPermissions("contract:list")
	@RequestMapping( "/list" )
	public String list() {
		return "contract/list";
	}

	/**
	 * 
	 * <p>描述：列表页ajax数据</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param searchEntity
	 * @param page
	 * @return
	 */
	@RequiresAuthentication
	@RequiresPermissions("contract:list")
	@RequestMapping( "/list/data" )
	@ResponseBody
	public JsonMessage listData(Contract searchEntity, @PageableDefault Pageable page) {
		Page<Contract> result = contractService.findPageList(searchEntity, page);
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS, result);
	}
	
	
	/**
	 * 
	 * <p>描述：删除，包含批量</p>
	 * <pre>
	 *    逻辑删除
	 * </pre>
	 * @param deleteList
	 * @return
	 * @throws BusinessException
	 */
	@ResponseBody
	@RequestMapping(value =  "/deleteAll" , method = RequestMethod.POST )
	public JsonMessage deleteAll(@RequestBody  List<Object> deleteList) throws BusinessException {
		contractService.delete4Logic(DeleteModelUtils.toDeleteModel(deleteList, Contract.class));
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	
	
}