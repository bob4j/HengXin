package com.hengxin.module.contract.service;

import org.springframework.validation.annotation.Validated;

import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.contract.entity.Product;
import com.hengxin.validated.group.Save;
import com.hengxin.validated.group.Update;

/**
 * <p>名称：ProductService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-3 下午1:37:48
 * @version 1.0.0
 */
@Validated
public interface ProductService extends BaseService<Product, Long> {
	
	
	public Product saveProduct(@Validated(Save.class) Product product);
	
	public Product updateProduct(@Validated(Update.class)  Product product) throws BusinessException;
	
}