/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.service</p>
 * <p>文件名：ContractTemplateService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-14-上午10:51:49</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.service;

import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.contract.entity.ContractTemplate;

/**<p>名称：ContractTemplateService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-14 上午10:51:49
 * @version 1.0.0
 */
public interface ContractTemplateService extends BaseService<ContractTemplate, Long>{

	/**
	 * 
	 * <p>描述：保存以及修改合同模板</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param contractTemplate
	 * @throws BusinessException
	 */
	public void saveOrUpdate(ContractTemplate contractTemplate, String path, String fileType) throws BusinessException;
	
	
	
}
