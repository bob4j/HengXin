package com.hengxin.module.contract.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Contractproduct entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "ContractProduct")
public class ContractProduct implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 3645646682634879830L;
	private Long id;
	private Long contractId;
	private Long productId;
	private Integer stockNum;

	// Constructors

	/** default constructor */
	public ContractProduct() {
	}
	

	/**
	 * <p>构造器：</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param id
	 * @param contractId
	 * @param productId
	 * @param stockNum
	 */
	public ContractProduct(Long id, Long contractId, Long productId,
			Integer stockNum) {
		super();
		this.id = id;
		this.contractId = contractId;
		this.productId = productId;
		this.stockNum = stockNum;
	}


	/** full constructor */
	public ContractProduct(Long contractId, Long productId) {
		this.contractId = contractId;
		this.productId = productId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "contractId", nullable = false)
	public Long getContractId() {
		return this.contractId;
	}

	public void setContractId(Long contractId) {
		this.contractId = contractId;
	}

	@Column(name = "productId", nullable = false)
	public Long getProductId() {
		return this.productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	/**
	 * @return the stockNum
	 */
	public Integer getStockNum() {
		return stockNum;
	}

	/**
	 * @param stockNum the stockNum to set
	 */
	@Column(name = "stockNum")
	public void setStockNum(Integer stockNum) {
		this.stockNum = stockNum;
	}

	
}