/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.service.impl</p>
 * <p>文件名：ContractServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:37:39</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import cn.singno.bob.common.utils.CalendarUtil;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.hengxin.constant.ApplicationContext;
import com.hengxin.constant.HengXinBusinessEnum;
import com.hengxin.constant.StateConstant;
import com.hengxin.module.contract.entity.Contract;
import com.hengxin.module.contract.repository.ContractRepository;
import com.hengxin.module.contract.service.ContractProductService;
import com.hengxin.module.contract.service.ContractService;

/**<p>名称：ContractServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:37:39
 * @version 1.0.0
 */
@Service
public class ContractServiceImpl extends BaseServiceImpl<Contract, Long> implements ContractService {

	
	@Autowired
	private ContractProductService contractProductService;
	
	@Autowired
	private ContractRepository contractRepository;
	
	/* (non-Javadoc)
	 * @see com.hengxin.module.contract.service.ContractService#saveContract(com.hengxin.module.contract.entity.Contract)
	 */
	@Override
	public void saveContract(Contract contract) throws BusinessException {
		long currentTime = CalendarUtil.getCurrentTime();
		contract.setCreateTime(currentTime);
		contract.setDelState(ApplicationContext.DEL_STATE_FALSE);
		contract.setUpdateTime(currentTime);
		contract.setState(StateConstant.CONTRACT_STATE_NOT_REPORTED);
		Contract nowCon = super.save(contract);
		//此处可以使用spring4的新特性，在对应的字段上加上  	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
		contract.setSignedDate(CalendarUtil.toLong(contract.getSigned(), CalendarUtil.FMT_Y_M_D));
		contract.setInstanStart(CalendarUtil.toLong(contract.getStart(), CalendarUtil.FMT_Y_M_D));
		contract.setInstanEnd(CalendarUtil.toLong(contract.getEnd(), CalendarUtil.FMT_Y_M_D));
		BigDecimal totalMoney = contractProductService.save(nowCon.getId(), contract.getProduct());
		nowCon.setTotalMoney(totalMoney);
		super.baseRepository.flush();
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.contract.service.ContractService#findById(java.lang.Long)
	 */
	@Override
	public Contract findById(Long id) {
		return contractRepository.findById(id, ApplicationContext.DEL_STATE_FALSE);
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.contract.service.ContractService#updateContract(com.hengxin.module.contract.entity.Contract)
	 */
	@Override
	public void updateContract(Contract contract) throws BusinessException {
		Contract old = this.findById(contract.getId());
		if(old == null){
			throw new BusinessException(HengXinBusinessEnum.RECORD_NOT_FIND);
		}
		contract.setId(old.getId());
		contract.setCreateTime(old.getCreateTime());
		contract.setDelState(old.getDelState());
		contract.setUpdateTime(CalendarUtil.getCurrentTime());
		//contract.setState(old.getState());
		contract.setSignedDate(CalendarUtil.toLong(contract.getSigned(), CalendarUtil.FMT_Y_M_D));
		contract.setInstanStart(CalendarUtil.toLong(contract.getStart(), CalendarUtil.FMT_Y_M_D));
		contract.setInstanEnd(CalendarUtil.toLong(contract.getEnd(), CalendarUtil.FMT_Y_M_D));
		super.updata(contract);
		contractProductService.deleteByContractId(contract.getId());
		BigDecimal totalMoney = contractProductService.save(contract.getId(), contract.getProduct());
		contract.setTotalMoney(totalMoney);
		super.baseRepository.flush();
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.contract.service.ContractService#delete4Logic(java.util.List)
	 */
	@Override
	public void delete4Logic(List<Contract> contracts) throws BusinessException {
		for (Contract contract : contracts) {
			Contract old = this.findById(contract.getId());
			if(old == null){
				throw new BusinessException(HengXinBusinessEnum.RECORD_NOT_FIND);
			}
			contractRepository.delete4Logic(old.getId(), CalendarUtil.getCurrentTime(), ApplicationContext.DEL_STATE_TRUE);
		}
		
	}
	

}
