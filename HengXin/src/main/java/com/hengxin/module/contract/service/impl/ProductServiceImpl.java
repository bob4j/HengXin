package com.hengxin.module.contract.service.impl;

import org.springframework.stereotype.Service;

import cn.singno.bob.common.utils.CalendarUtil;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.hengxin.constant.ApplicationContext;
import com.hengxin.constant.HengXinBusinessEnum;
import com.hengxin.module.contract.entity.Product;
import com.hengxin.module.contract.service.ProductService;

/**
 * <p>名称：ProductServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-3 下午1:42:53
 * @version 1.0.0
 */
@Service
public class ProductServiceImpl extends BaseServiceImpl<Product, Long>  implements ProductService{
	


	/* (non-Javadoc)
	 * @see com.hengxin.module.contract.service.ProductService#update(com.hengxin.module.contract.entity.Product)
	 */
	@Override
	public Product updateProduct(Product product) throws BusinessException {
		Product old = (Product) super.findOne(product.getId());
		if (old == null) {
			throw new BusinessException(HengXinBusinessEnum.RECORD_NOT_FIND);
		}
		product.setCreateTime(old.getCreateTime());
		product.setDelState(old.getDelState());
		return super.updata(product);
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.contract.service.ProductService#saveProduct(com.hengxin.module.contract.entity.Product)
	 */
	@Override
	public Product saveProduct( Product product) {
		product.setCreateTime(Long.valueOf(CalendarUtil.getCurrentTime()));
		product.setDelState(ApplicationContext.DEL_STATE_FALSE);
		return super.save(product);
	}
}