/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.service</p>
 * <p>文件名：ContractService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:37:10</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.service;

import java.util.List;

import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.contract.entity.Contract;

/**<p>名称：ContractService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:37:10
 * @version 1.0.0
 */
public interface ContractService extends BaseService<Contract, Long>{

	/**
	 * 
	 * <p>描述：保存</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param contract
	 * @throws BusinessException
	 */
	public void saveContract(Contract contract) throws BusinessException;
	
	/**
	 * 
	 * <p>描述：修改</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param contract
	 * @throws BusinessException
	 */
	public void updateContract(Contract contract) throws BusinessException;
	
	
	/**
	 * 通过ID来查询，排除已经删除的记录
	 */
	public Contract findById(Long id);
	
	/**
	 * 
	 * <p>描述：逻辑删除</p>
	 * <pre>
	 *    
	 * </pre>
	 */
	public void delete4Logic(List<Contract> contracts) throws BusinessException;
	
}
