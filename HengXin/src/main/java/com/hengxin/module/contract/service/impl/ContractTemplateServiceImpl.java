/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.service.impl</p>
 * <p>文件名：ContractTemplateServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-14-上午10:52:10</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.singno.bob.common.utils.CalendarUtil;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.hengxin.module.contract.entity.ContractTemplate;
import com.hengxin.module.contract.repository.ContractTemplateRepository;
import com.hengxin.module.contract.service.ContractTemplateService;

/**<p>名称：ContractTemplateServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-14 上午10:52:10
 * @version 1.0.0
 */
@Service
public class ContractTemplateServiceImpl extends BaseServiceImpl<ContractTemplate, Long> implements ContractTemplateService{

	
	@Autowired
	private ContractTemplateRepository contractTemplateRepository;
	
	/* (non-Javadoc)
	 * @see com.hengxin.module.contract.service.ContractTemplateService#saveOrUpdate(com.hengxin.module.contract.entity.ContractTemplate)
	 */
	@Override
	public void saveOrUpdate(ContractTemplate contractTemplate, String path, String fileType)
			throws BusinessException {
		ContractTemplate old = super.findOne(contractTemplate.getId());
		if( old == null){			//保存
			save(contractTemplate, path, fileType);
		}else{						//修改
			update(contractTemplate, old, path, fileType);
		}
	}

	//保存
	private void save(ContractTemplate contractTemplate, String path, String fileType){
		if(contractTemplate.getIsDefualt()){					//如果当前这个模板设为默认时的操作
			ContractTemplate defaultT = contractTemplateRepository.findDefaultTemplate();
			if(defaultT != null){
				defaultT.setIsDefualt(false);
				super.updata(defaultT);
			}
		}
		contractTemplate.setCreateTime(CalendarUtil.getCurrentTime());
		contractTemplate.setUri(path);
		contractTemplate.setFileType(fileType);
		super.save(contractTemplate);
	}
	
	//修改
	private void update(ContractTemplate contractTemplate, ContractTemplate old, String path, String fileType){
		if(contractTemplate.getIsDefualt()){						//修改时检测设默认操作
			ContractTemplate defaultT = contractTemplateRepository.findDefaultTemplate();
			if( !old.equals(defaultT.getId())){
				if(defaultT != null){
					defaultT.setIsDefualt(false);
					super.updata(defaultT);
				}
			}
		}
		if(StringUtils.isBlank(path) || StringUtils.isBlank(fileType)){
			contractTemplate.setUri(old.getUri());
			contractTemplate.setFileType(old.getFileType());
		}else{
			contractTemplate.setUri(path);
			contractTemplate.setFileType(fileType);
		}
		contractTemplate.setCreateTime(old.getCreateTime());
		super.updata(contractTemplate);
	}
	
}
