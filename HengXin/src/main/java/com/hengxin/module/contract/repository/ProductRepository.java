package com.hengxin.module.contract.repository;

import cn.singno.bob.jpa.base.BaseRepository;
import com.hengxin.module.contract.entity.Product;

/**
 * <p>名称：ProductRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-3 下午1:37:01
 * @version 1.0.0
 */
public interface ProductRepository extends BaseRepository<Product, Long>{
	
}