/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.repository</p>
 * <p>文件名：ContractTemplateRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-14-上午10:51:14</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.repository;

import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.contract.entity.ContractTemplate;

/**<p>名称：ContractTemplateRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-14 上午10:51:14
 * @version 1.0.0
 */
public interface ContractTemplateRepository extends BaseRepository<ContractTemplate, Long> {

	
	@Query("select o from ContractTemplate o where o.isDefualt = 1")
	public ContractTemplate findDefaultTemplate();
	
}
