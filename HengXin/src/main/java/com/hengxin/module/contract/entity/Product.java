package com.hengxin.module.contract.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.hengxin.module.contract.model.ProductModel;
import com.hengxin.validated.group.Save;
import com.hengxin.validated.group.Update;

/**
 * <p>
 * 名称：Product.java
 * </p>
 * <p>
 * 描述：
 * </p>
 * 
 * <pre>
 * 
 * </pre>
 * 
 * @author 鲍建明
 * @date 2014-11-3 下午1:35:36
 * @version 1.0.0
 */
@Entity
@Table(name = "product")
public class Product extends ProductModel implements Serializable {

	private static final long serialVersionUID = -2183329685939646476L;
	@NotNull(message="产品ID不能为空", groups={Update.class})
	private Long id;			//主键
	
	@NotBlank(message="产品编号不能为空", groups={Save.class,Update.class})
	private String number;		//产品编号
	
	@NotBlank(message="产品规格不能为空", groups={Save.class,Update.class})
	private String standard;	//产品规格
	
	@NotBlank(message="电压不能为空", groups={Save.class,Update.class})
	private String voltage;		//电压
	
	@NotNull(message="销售单价不能为空", groups={Save.class,Update.class})
	private BigDecimal salePrice;	//销售单价
	
	@NotNull(message="进货单价不能为空", groups={Save.class,Update.class})
	private BigDecimal stockPrice;	//进货单价

	@NotBlank(message="产品单位不能为空", groups={Save.class,Update.class})
	private String unit;			//单位
	
	private Long createTime;
	
	private Boolean delState; 
	
	private Integer stock;			//库存

	public Product() {
	}

	public Product(Long id, Boolean delState) {
		this.id = id;
		this.delState = delState;
	}

	public Product(Long id, String number, String standard, String voltage,
			BigDecimal salePrice, BigDecimal stockPrice, String unit, Boolean delState) {
		this.id = id;
		this.number = number;
		this.standard = standard;
		this.voltage = voltage;
		this.salePrice = salePrice;
		this.stockPrice = stockPrice;
		this.unit = unit;
		this.delState = delState;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "number", length = 20)
	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name = "standard", length = 20)
	public String getStandard() {
		return this.standard;
	}

	public void setStandard(String standard) {
		this.standard = standard;
	}

	@Column(name = "voltage", length = 20)
	public String getVoltage() {
		return this.voltage;
	}

	public void setVoltage(String voltage) {
		this.voltage = voltage;
	}




	@Column(name = "unit", length = 10)
	public String getUnit() {
		return this.unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Column(name = "delState", nullable = false)
	public Boolean getDelState() {
		return this.delState;
	}

	public void setDelState(Boolean delState) {
		this.delState = delState;
	}

	@Column(name = "createTime", nullable = false)
	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the stock
	 */
	@Column(name = "stock")
	public Integer getStock() {
		return stock;
	}

	/**
	 * @param stock the stock to set
	 */
	public void setStock(Integer stock) {
		this.stock = stock;
	}

	/**
	 * @return the salePrice
	 */
	@Column(name = "salePrice")
	public BigDecimal getSalePrice() {
		return salePrice;
	}

	/**
	 * @param salePrice the salePrice to set
	 */
	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	/**
	 * @return the stockPrice
	 */
	@Column(name = "stockPrice")
	public BigDecimal getStockPrice() {
		return stockPrice;
	}

	/**
	 * @param stockPrice the stockPrice to set
	 */
	public void setStockPrice(BigDecimal stockPrice) {
		this.stockPrice = stockPrice;
	}
	
	
}