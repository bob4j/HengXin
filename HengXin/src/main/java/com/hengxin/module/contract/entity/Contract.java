package com.hengxin.module.contract.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import cn.singno.bob.jpa.annotation.search.Search;

import com.hengxin.module.contract.model.ContractModel;

/**
 * Contract entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "contract")
public class Contract extends ContractModel implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9110515938603761202L;
	// Fields

	private Long id;
	private String contractNo;	//合同编号
	@Search(Like="#")
	private String partyA;		//甲方
	private String partyB;		//乙方
	private String signedAddre;	//签订地点
	private String useFor;		//用途	
	private String instanAddre;	//安装地点
	private String bearMethod;	//运输方式及费用负担
	private String chargeStandard;	//配管、开孔和支架收费标准
	private String objectionDate;	//异议期限
	private String checkDate;		//验收日期
	private String payMent;			//结算方式及期限
	private String breachContract;	//违约责任
	private String otherItem;		//其他约定事项
	private String proxyA;			//甲方代理人
	private String proxyB;			//乙方代理人
	private String telA;			//甲方联系电话
	private String telB;			//乙方联系电话
	private String phoneA;			//甲方手机号码
	private String phoneB;			//乙方手机号码
	private Long signedDate;		//合同签订时期
	@Search(EQ="#")
	private Boolean isMode;			//0:范本 1：合同
	@Search(EQ="#")
	private Short state;			//合同的状态	1.未报　2.已报　3.未批　　4.资料上交 　5.工程返利
	private BigDecimal totalMoney;		//合计
	private Long instanStart;		//安装时间开始
	private Long instanEnd;			//安装时间结束
	private Long createTime;		//创建时间
	private Long updateTime;		//修改时间
	
	@Search(EQ="${ com.hengxin.constant.ApplicationContext.DEL_STATE_FALSE}")
	private Boolean delState;		//0:没删除   1：删除

	// Constructors

	/** default constructor */
	public Contract() {
	}

	/** minimal constructor */
	public Contract(String contractNo, Long createTime, Long updateTime,
			Boolean delState) {
		this.contractNo = contractNo;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.delState = delState;
	}

	/** full constructor */
	public Contract(String contractNo, String partyA, String partyB,
			String signedAddre, String useFor, String instanAddre,
			String bearMethod, String chargeStandard, String objectionDate,
			String checkDate, String payMent, String breachContract,
			String otherItem, String proxyA, String proxyB, String telA,
			String telB, String phoneA, String phoneB, Long signedDate,
			Boolean isMode, Short state, BigDecimal totalMoney, Long instanStart,
			Long instanEnd, Long createTime, Long updateTime, Boolean delState) {
		this.contractNo = contractNo;
		this.partyA = partyA;
		this.partyB = partyB;
		this.signedAddre = signedAddre;
		this.useFor = useFor;
		this.instanAddre = instanAddre;
		this.bearMethod = bearMethod;
		this.chargeStandard = chargeStandard;
		this.objectionDate = objectionDate;
		this.checkDate = checkDate;
		this.payMent = payMent;
		this.breachContract = breachContract;
		this.otherItem = otherItem;
		this.proxyA = proxyA;
		this.proxyB = proxyB;
		this.telA = telA;
		this.telB = telB;
		this.phoneA = phoneA;
		this.phoneB = phoneB;
		this.signedDate = signedDate;
		this.isMode = isMode;
		this.state = state;
		this.totalMoney = totalMoney;
		this.instanStart = instanStart;
		this.instanEnd = instanEnd;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.delState = delState;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "contractNo", nullable = false, length = 15)
	public String getContractNo() {
		return this.contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	@Column(name = "partyA", length = 30)
	public String getPartyA() {
		return this.partyA;
	}

	public void setPartyA(String partyA) {
		this.partyA = partyA;
	}

	@Column(name = "partyB", length = 30)
	public String getPartyB() {
		return this.partyB;
	}

	public void setPartyB(String partyB) {
		this.partyB = partyB;
	}

	@Column(name = "signedAddre", length = 60)
	public String getSignedAddre() {
		return this.signedAddre;
	}

	public void setSignedAddre(String signedAddre) {
		this.signedAddre = signedAddre;
	}

	@Column(name = "useFor", length = 40)
	public String getUseFor() {
		return this.useFor;
	}

	public void setUseFor(String useFor) {
		this.useFor = useFor;
	}

	@Column(name = "instanAddre", length = 60)
	public String getInstanAddre() {
		return this.instanAddre;
	}

	public void setInstanAddre(String instanAddre) {
		this.instanAddre = instanAddre;
	}

	@Column(name = "bearMethod", length = 40)
	public String getBearMethod() {
		return this.bearMethod;
	}

	public void setBearMethod(String bearMethod) {
		this.bearMethod = bearMethod;
	}

	@Column(name = "chargeStandard", length = 40)
	public String getChargeStandard() {
		return this.chargeStandard;
	}

	public void setChargeStandard(String chargeStandard) {
		this.chargeStandard = chargeStandard;
	}

	@Column(name = "objectionDate", length = 30)
	public String getObjectionDate() {
		return this.objectionDate;
	}

	public void setObjectionDate(String objectionDate) {
		this.objectionDate = objectionDate;
	}

	@Column(name = "checkDate", length = 20)
	public String getCheckDate() {
		return this.checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	@Column(name = "payMent", length = 30)
	public String getPayMent() {
		return this.payMent;
	}

	public void setPayMent(String payMent) {
		this.payMent = payMent;
	}

	@Column(name = "breachContract", length = 500)
	public String getBreachContract() {
		return this.breachContract;
	}

	public void setBreachContract(String breachContract) {
		this.breachContract = breachContract;
	}

	@Column(name = "otherItem", length = 500)
	public String getOtherItem() {
		return this.otherItem;
	}

	public void setOtherItem(String otherItem) {
		this.otherItem = otherItem;
	}

	@Column(name = "proxyA", length = 30)
	public String getProxyA() {
		return this.proxyA;
	}

	public void setProxyA(String proxyA) {
		this.proxyA = proxyA;
	}

	@Column(name = "proxyB", length = 30)
	public String getProxyB() {
		return this.proxyB;
	}

	public void setProxyB(String proxyB) {
		this.proxyB = proxyB;
	}

	@Column(name = "telA", length = 30)
	public String getTelA() {
		return this.telA;
	}

	public void setTelA(String telA) {
		this.telA = telA;
	}

	@Column(name = "telB", length = 30)
	public String getTelB() {
		return this.telB;
	}

	public void setTelB(String telB) {
		this.telB = telB;
	}

	@Column(name = "phoneA", length = 30)
	public String getPhoneA() {
		return this.phoneA;
	}

	public void setPhoneA(String phoneA) {
		this.phoneA = phoneA;
	}

	@Column(name = "phoneB", length = 30)
	public String getPhoneB() {
		return this.phoneB;
	}

	public void setPhoneB(String phoneB) {
		this.phoneB = phoneB;
	}

	@Column(name = "signedDate")
	public Long getSignedDate() {
		return this.signedDate;
	}

	public void setSignedDate(Long signedDate) {
		this.signedDate = signedDate;
	}

	@Column(name = "isMode")
	public Boolean getIsMode() {
		return this.isMode;
	}

	public void setIsMode(Boolean isMode) {
		this.isMode = isMode;
	}

	@Column(name = "state")
	public Short getState() {
		return this.state;
	}

	public void setState(Short state) {
		this.state = state;
	}

	@Column(name = "totalMoney", precision = 8)
	public BigDecimal getTotalMoney() {
		return this.totalMoney;
	}

	public void setTotalMoney(BigDecimal totalMoney) {
		this.totalMoney = totalMoney;
	}

	@Column(name = "instanStart")
	public Long getInstanStart() {
		return this.instanStart;
	}

	public void setInstanStart(Long instanStart) {
		this.instanStart = instanStart;
	}

	@Column(name = "instanEnd")
	public Long getInstanEnd() {
		return this.instanEnd;
	}

	public void setInstanEnd(Long instanEnd) {
		this.instanEnd = instanEnd;
	}

	@Column(name = "createTime", nullable = false)
	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	@Column(name = "updateTime", nullable = false)
	public Long getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "delState", nullable = false)
	public Boolean getDelState() {
		return this.delState;
	}

	public void setDelState(Boolean delState) {
		this.delState = delState;
	}

}