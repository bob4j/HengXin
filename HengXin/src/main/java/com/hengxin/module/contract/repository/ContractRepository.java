/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.contract.repository</p>
 * <p>文件名：ContractRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:36:49</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.contract.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.contract.entity.Contract;

/**<p>名称：ContractRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:36:49
 * @version 1.0.0
 */
public interface ContractRepository extends BaseRepository<Contract, Long> {

	/**
	 * 
	 * <p>描述：通过ID来查询  删除或非删除状态的合同</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param id
	 * @param delState
	 * @return
	 */
	@Query("select o from Contract o where o.id = ?1 and o.delState = ?2 ")
	public Contract findById(Long id, Boolean delState);
	
	/**
	 * 
	 * <p>描述：通过ID来逻辑删除合同</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param id
	 * @param updateTime
	 * @param delState
	 */
	@Modifying
	@Query("update Contract o set o.updateTime = ?2, o.delState = ?3 where o.id = ?1")
	public void delete4Logic(Long id, Long updateTime, Boolean delState);
	
}
