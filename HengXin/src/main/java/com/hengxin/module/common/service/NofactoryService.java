/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service</p>
 * <p>文件名：NofactoryService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:08:59</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.service;


import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.module.common.entity.Nofactory;

/**<p>名称：NofactoryService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:08:59
 * @version 1.0.0
 */
public interface NofactoryService extends BaseService<Nofactory, Long>{

	/**
	 * 
	 * <p>描述：创建一个合同编号</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	public Nofactory buildContractNo();
	
}
