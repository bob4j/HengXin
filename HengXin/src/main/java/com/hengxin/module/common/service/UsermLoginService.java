/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service</p>
 * <p>文件名：UsermLoginService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-10-下午1:27:41</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.service;

import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.bean.ShiroUserm;
import com.hengxin.module.common.entity.UsermLogin;

/**<p>名称：UsermLoginService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-10 下午1:27:41
 * @version 1.0.0
 */
public interface UsermLoginService extends BaseService<UsermLogin, Long>{

	/**
	 * 
	 * <p>描述：保存用户登录信息</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param userm
	 */
	public void save(ShiroUserm shiroUserm, String ipAddr);
	
}
