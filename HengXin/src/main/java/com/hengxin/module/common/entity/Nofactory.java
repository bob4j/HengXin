package com.hengxin.module.common.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Nofactory entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "nofactory")
public class Nofactory implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -460427643980791428L;
	private Long id;
	private String number;
	private Short useFor;
	private String metaDesc;
	private Long createTime;

	// Constructors

	/** default constructor */
	public Nofactory() {
	}

	/** minimal constructor */
	public Nofactory(String number, Short useFor, Long createTime) {
		this.number = number;
		this.useFor = useFor;
		this.createTime = createTime;
	}

	/** full constructor */
	public Nofactory(String number, Short useFor, String metaDesc,
			Long createTime) {
		this.number = number;
		this.useFor = useFor;
		this.metaDesc = metaDesc;
		this.createTime = createTime;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "number", nullable = false, length = 20)
	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	@Column(name = "useFor", nullable = false)
	public Short getUseFor() {
		return this.useFor;
	}

	public void setUseFor(Short useFor) {
		this.useFor = useFor;
	}

	@Column(name = "metaDesc", length = 60)
	public String getMetaDesc() {
		return this.metaDesc;
	}

	public void setMetaDesc(String metaDesc) {
		this.metaDesc = metaDesc;
	}

	@Column(name = "createTime", nullable = false)
	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

}