package com.hengxin.module.common.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Userm entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "userm")
public class Userm implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 3136541366710900373L;
	private Long id;
	private String name;
	private String account;
	
	private String pwd;
	private String salt;
	private Integer age;
	private Short sex;
	private String email;
	private Short userState;   //用户状态 0:正常状态 1:冻结状态
	private Long createTime;
	private Long updateTime;

	// Constructors

	/** default constructor */
	public Userm() {
	}

	/** minimal constructor */
	public Userm(String name, String account, String pwd, String salt,
			Short userState, Long createTime, Long updateTime) {
		this.name = name;
		this.account = account;
		this.pwd = pwd;
		this.salt = salt;
		this.userState = userState;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	/** full constructor */
	public Userm(String name, String account, String pwd, String salt,
			Integer age, Short sex, Short userState, Long createTime,
			Long updateTime) {
		this.name = name;
		this.account = account;
		this.pwd = pwd;
		this.salt = salt;
		this.age = age;
		this.sex = sex;
		this.userState = userState;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "name", nullable = false, length = 30)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "account", nullable = false, length = 30)
	public String getAccount() {
		return this.account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	@Column(name = "pwd", nullable = false, length = 60)
	@JSONField(serialize=false)
	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Column(name = "salt", nullable = false, length = 4)
	@JSONField(serialize=false)
	public String getSalt() {
		return this.salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	@Column(name = "age")
	public Integer getAge() {
		return this.age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Column(name = "sex")
	public Short getSex() {
		return this.sex;
	}

	public void setSex(Short sex) {
		this.sex = sex;
	}

	@Column(name = "userState", nullable = false)
	public Short getUserState() {
		return this.userState;
	}

	public void setUserState(Short userState) {
		this.userState = userState;
	}

	@Column(name = "createTime", nullable = false)
	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

	@Column(name = "updateTime", nullable = false)
	public Long getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	/**
	 * @return the email
	 */
	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	

}