/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service.impl</p>
 * <p>文件名：NofactoryServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:09:13</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.singno.bob.common.utils.CalendarUtil;
import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.hengxin.constant.ApplicationContext;
import com.hengxin.constant.StateConstant;
import com.hengxin.module.common.entity.Nofactory;
import com.hengxin.module.common.repository.NofactoryRepository;
import com.hengxin.module.common.service.NofactoryService;
import com.hengxin.utils.RandomUtils;

/**<p>名称：NofactoryServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:09:13
 * @version 1.0.0
 */
@Service
public class NofactoryServiceImpl extends BaseServiceImpl<Nofactory, Long> implements NofactoryService{

	@Autowired
	private NofactoryRepository nofactoryRepository;
	
	/* (non-Javadoc)
	 * @see com.hengxin.module.common.service.NofactoryService#buildContractNo()
	 */
	@Override
	public Nofactory buildContractNo() {
		Nofactory nofactory = new Nofactory();
		nofactory.setCreateTime(CalendarUtil.getCurrentTime());
		nofactory.setUseFor(StateConstant.NOFACTORY_USEFOR_CONTRACT);
		String maxNo = nofactoryRepository.findMaxNo( CalendarUtil.getMillisOfDay(0), CalendarUtil.getMillisOfDay(1));
		String nextNo = RandomUtils.getNextNo(ApplicationContext.CONTRACT_PREFIX_NO, maxNo);
		nofactory.setNumber(nextNo);
		nofactory.setMetaDesc("合同编号");
		return super.save(nofactory);
	}


	
}
