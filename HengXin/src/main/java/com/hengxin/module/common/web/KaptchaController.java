/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.web</p>
 * <p>文件名：KaptchaController.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-8-下午2:29:45</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.web;

import java.awt.image.BufferedImage;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.singno.bob.web.base.BaseController;

import com.google.code.kaptcha.Producer;

/**<p>名称：KaptchaController.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-8 下午2:29:45
 * @version 1.0.0
 */
@Controller
@RequestMapping()
public class KaptchaController extends BaseController {

	
public static final String KAPTCHA = "kaptcha";
	
	
	@Resource(name="captchaProducer")
	private Producer        captchaProducer;
	


	@RequestMapping("/getCaptcha")
    public ModelAndView getCaptcha(HttpServletRequest request,
            HttpServletResponse response) throws Exception
    {
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control",
                "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Prama", "no-cache");
        response.setContentType("image/jpeg");
        String captext = captchaProducer.createText();
        request.getSession().setAttribute(KAPTCHA, captext);
       // cacheManager.getCache("passwordRetryCache").put(KAPTCHA, captext);
        BufferedImage bi = captchaProducer.createImage(captext);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(bi, "jpg", out);
        try
        {
            out.flush();
        }
        finally
        {
            out.close();
        }
        return null;
    }
	
}
