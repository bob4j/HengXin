/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.repository</p>
 * <p>文件名：UsermLoginRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-10-下午1:27:34</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.repository;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.common.entity.UsermLogin;

/**<p>名称：UsermLoginRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-10 下午1:27:34
 * @version 1.0.0
 */
public interface UsermLoginRepository extends BaseRepository<UsermLogin, Long>{

}
