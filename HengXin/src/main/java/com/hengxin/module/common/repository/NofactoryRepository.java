/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.repository</p>
 * <p>文件名：NofactoryRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:08:29</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.repository;

import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.common.entity.Nofactory;

/**<p>名称：NofactoryRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:08:29
 * @version 1.0.0
 */
public interface NofactoryRepository extends BaseRepository<Nofactory, Long> {

	@Query("select max(o.number) from Nofactory o where o.createTime between ?1 and ?2")
	public String findMaxNo(long now, long tomorrow);
	
}
