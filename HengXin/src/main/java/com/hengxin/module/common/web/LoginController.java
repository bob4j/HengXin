/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.web</p>
 * <p>文件名：LoginController.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-8-下午2:02:44</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.web;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.singno.bob.web.base.BaseController;

import com.hengxin.constant.ApplicationContext;
import com.hengxin.module.common.service.UsermLoginService;

/**<p>名称：LoginController.java</p>
 * <p>描述：登陆的控制器</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-8 下午2:02:44
 * @version 1.0.0
 */
@Controller
@RequestMapping(ApplicationContext.COMMON_MODULE)
public class LoginController extends BaseController{

	@Autowired
	private UsermLoginService usermLoginService;
	
	@RequestMapping("/login")
	public String login(){
		return "login";
	}
	
	/**
	 * 
	 * <p>描述：</p>
	 * <pre>
	 *    进入该跳转器时,说明登录已经出现异常
	 * </pre>
	 * @return
	 */
	@RequestMapping(value="/submitLogin",  method = RequestMethod.POST)
	public String loginFail(){
		return "login";
	}
	
	/**
	 * 
	 * <p>描述：推出登陆</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	@RequestMapping(value="/loginout")
	public String loginout(){
		SecurityUtils.getSubject().logout();
		return "redirect:common/login";
	}
	
}
