/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service</p>
 * <p>文件名：UsermService.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-9-下午12:10:16</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.BaseService;

import com.hengxin.bean.ShiroUserm;
import com.hengxin.module.common.entity.Userm;
import com.hengxin.module.common.model.UsermModel;

/**<p>名称：UsermService.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-9 下午12:10:16
 * @version 1.0.0
 */
public interface UsermService extends BaseService<Userm, Long>{

	/**
	 * 
	 * <p>描述：通过账号来查询用户</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param account
	 * @return
	 */
	public Userm findByAccount(String account);
	
	/**
	 * 
	 * <p>描述：获取当前用户信息</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	public ShiroUserm getCurrentUserm();
	
	/**
	 * 
	 * <p>描述：修改密码</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param id
	 * @param newPwd
	 */
	public void updatePwd(Long id, String oldPwd, String newPwd, String salt) throws BusinessException ;
	
	/**
	 * 
	 * <p>描述：保存用户</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param usermModel
	 * @return
	 */
	public Userm saveUserm(UsermModel usermModel) throws BusinessException;
	
	/**
	 * 
	 * <p>描述：查询该账号是否已经使用</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param account
	 * @return
	 */
	public boolean isExist(String account);
	
	/**
	 * 动态分页查询
	 * <p>描述：</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param userm
	 * @return
	 */
	public Page<Userm> searchByConditions(UsermModel usermModel, Long organIds, Pageable page);
	
	
}
