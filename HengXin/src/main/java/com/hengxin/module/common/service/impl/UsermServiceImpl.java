/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service.impl</p>
 * <p>文件名：UsermServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-9-下午12:10:36</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cn.singno.bob.common.utils.CalendarUtil;
import cn.singno.bob.common.utils.EncryptUtil;
import cn.singno.bob.common.utils.EntityUtils;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.hengxin.bean.ShiroUserm;
import com.hengxin.constant.HengXinBusinessEnum;
import com.hengxin.constant.StateConstant;
import com.hengxin.module.common.entity.Userm;
import com.hengxin.module.common.model.UsermModel;
import com.hengxin.module.common.repository.UsermRepository;
import com.hengxin.module.common.service.UsermService;
import com.hengxin.module.setting.entity.Role;
import com.hengxin.module.setting.service.RoleService;
import com.hengxin.module.setting.service.UsermOrganizationService;
import com.hengxin.module.setting.service.UsermRoleService;
import com.hengxin.utils.RandomUtils;

/**<p>名称：UsermServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-9 下午12:10:36
 * @version 1.0.0
 */
@Service
public class UsermServiceImpl extends BaseServiceImpl<Userm, Long> implements UsermService {

	private static final String DEFAULT_PWD = "123456";
	
	@Autowired
	private UsermRepository usermRepository;
	
	@Autowired
	private UsermRoleService usermRoleService;
	
	@Autowired
	private UsermOrganizationService usermOrganizationService;
	
	@Autowired
	private RoleService roleService;
	
	/* (non-Javadoc)
	 * @see com.hengxin.module.common.service.UsermService#findByAccount(java.lang.String)
	 */
	@Override
	public Userm findByAccount(String account) {
		if(StringUtils.isBlank(account)){
			return null;
		}
		return usermRepository.findByAccount(account);
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.common.service.UsermService#getCurrentUserm()
	 */
	@Override
	public ShiroUserm getCurrentUserm() {
		return (ShiroUserm) SecurityUtils.getSubject().getPrincipal();
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.common.service.UsermService#updatePwd(java.lang.Long, java.lang.String)
	 */
	@Override
	public void updatePwd(Long id, String oldPwd, String newPwd, String salt) throws BusinessException {
		Userm userm = usermRepository.findById(id, StateConstant.USERM_STATE_RIGHT);
		if(userm == null){
			throw new BusinessException(HengXinBusinessEnum.USERM_NOT_FIND);
		}
		oldPwd = EncryptUtil.customEncrypt(oldPwd, salt);
		if( !(userm.getPwd().equals(oldPwd)) ){
			throw new BusinessException(HengXinBusinessEnum.USERM_PWD_ERROR);
		}
		String pwd = EncryptUtil.customEncrypt(newPwd, salt);
		usermRepository.updatePwd(id, pwd);
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.common.service.UsermService#saveUserm(com.hengxin.module.common.model.UsermModel)
	 */
	@Override
	public Userm saveUserm(UsermModel usermModel) throws BusinessException {
		Role role = roleService.findOne(usermModel.getRoleId());
		if(role == null || !role.getOrganizationId().equals(usermModel.getOrganizationId())){
			throw new BusinessException(HengXinBusinessEnum.ROLE_INFO_NOT_FIND);
		}
		if( this.isExist(usermModel.getAccount()) ){
			throw new BusinessException(HengXinBusinessEnum.USERM_ACCOUNT_EXIST);
		}
		Userm userm = new Userm();
		EntityUtils.copyBean(userm, usermModel);
		Long time = CalendarUtil.getCurrentTime();
		userm.setCreateTime(time);
		userm.setUpdateTime(time);
		userm.setUserState(usermModel.getUserState());
		String salt = RandomUtils.getRandom();
		userm.setSalt(salt);
		String pwd = EncryptUtil.customEncrypt(DEFAULT_PWD, salt);
		userm.setPwd(pwd);
		userm = super.save(userm);
		usermRoleService.save(userm.getId(), usermModel.getRoleId());
		usermOrganizationService.save(userm.getId(), usermModel.getOrganizationId());
		return userm;
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.common.service.UsermService#isExist(java.lang.String)
	 */
	@Override
	public boolean isExist(String account) {
		return usermRepository.isExist(account) == null ? false : true;
	}

	/* (non-Javadoc)
	 * @see com.hengxin.module.common.service.UsermService#searchByConditions(com.hengxin.module.common.entity.Userm)
	 */
	@Override
	public Page<Userm> searchByConditions(UsermModel usermModel, Long organIds, Pageable page) {
		return usermRepository.searchByConditions(usermModel, organIds, page);
	}

}
