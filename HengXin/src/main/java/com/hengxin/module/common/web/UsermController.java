/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.web</p>
 * <p>文件名：UsermController.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-10-上午11:04:59</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.web;

import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.singno.bob.bean.JsonMessage;
import cn.singno.bob.emun.CommonEmun;
import cn.singno.bob.exception.BusinessException;
import cn.singno.bob.jpa.utils.JsonMessageUtils;
import cn.singno.bob.web.base.BaseController;

import com.alibaba.fastjson.JSON;
import com.hengxin.bean.ShiroUserm;
import com.hengxin.constant.ApplicationContext;
import com.hengxin.module.common.entity.Userm;
import com.hengxin.module.common.model.UsermModel;
import com.hengxin.module.common.service.UsermService;
import com.hengxin.module.setting.model.ComboTree;
import com.hengxin.module.setting.service.OrganizationService;
import com.hengxin.validated.group.Update;

/**<p>名称：UsermController.java</p>
 * <p>描述：</p>
 * <pre>
 *    用户控制器
 * </pre>
 * @author 鲍建明
 * @date 2014-11-10 上午11:04:59
 * @version 1.0.0
 */
@Controller
@RequestMapping(ApplicationContext.USERM_MODULE)
public class UsermController extends BaseController{

	@Autowired
	private UsermService usermService;
	
	
	@Autowired
	private OrganizationService organizationService;
	
	/**
	 * 
	 * <p>描述：修改密码</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param oldPwd
	 * @param newPwd
	 * @return
	 * @throws BusinessException 
	 */
	@RequiresAuthentication
	@RequestMapping(value = "/updatePwd", method = RequestMethod.POST)
	@ResponseBody
	public JsonMessage updatePwd(@Validated(value=Update.class) UsermModel usermModel) throws BusinessException{
		ShiroUserm shiroUserm = usermService.getCurrentUserm();
		usermService.updatePwd(shiroUserm.getId(), usermModel.getOldPwd(), usermModel.getNewPwd(), shiroUserm.getSalt());
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	@RequestMapping(value = "/load")
	public String load(Long id, Model model){
		ShiroUserm shiroUserm = usermService.getCurrentUserm();
		ComboTree organizationTree = organizationService.getComboTree(shiroUserm.getOrganId());
		model.addAttribute("organizationTree", JSON.toJSONString(organizationTree));
		return "userm/add";
	}
	
	/**
	 * 
	 * <p>描述：用户新增</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param usermModel
	 * @return
	 * @throws BusinessException 
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@ResponseBody
	public JsonMessage save(@RequestBody UsermModel usermModel) throws BusinessException{
		usermService.saveUserm(usermModel);
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
	/**
	 * 
	 * <p>描述：用户列表</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	@RequestMapping(value = "/list")
	public String list(){
		return "userm/list";
	}
	
	/**
	 * 
	 * <p>描述：数据</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param usermLogin
	 * @param page
	 * @return
	 */
	@RequestMapping(value = "/list/data")
	@ResponseBody
	public JsonMessage listData(UsermModel usermModel, Pageable page){
		ShiroUserm shiroUserm = usermService.getCurrentUserm();
		Page<Userm> result = usermService.searchByConditions(usermModel,shiroUserm.getOrganId(), page);
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS, result);
	}
	
	
	
}
