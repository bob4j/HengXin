/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.service.impl</p>
 * <p>文件名：UsermLoginServiceImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-10-下午1:27:50</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.service.impl;

import org.springframework.stereotype.Service;

import cn.singno.bob.common.utils.CalendarUtil;
import cn.singno.bob.jpa.base.impl.BaseServiceImpl;

import com.hengxin.bean.ShiroUserm;
import com.hengxin.module.common.entity.UsermLogin;
import com.hengxin.module.common.service.UsermLoginService;

/**<p>名称：UsermLoginServiceImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-10 下午1:27:50
 * @version 1.0.0
 */
@Service
public class UsermLoginServiceImpl extends BaseServiceImpl<UsermLogin, Long> implements UsermLoginService{

	/* (non-Javadoc)
	 * @see com.hengxin.module.common.service.UsermLoginService#save(com.hengxin.module.common.entity.Userm)
	 */
	@Override
	public void save(ShiroUserm shiroUserm, String ipAddr) {
		UsermLogin old = super.findOne(shiroUserm.getId());
		UsermLogin usermLogin = new UsermLogin();
		usermLogin.setUserId(shiroUserm.getId());
		usermLogin.setCurrentIp(ipAddr);
		usermLogin.setCurrentLoginTime(CalendarUtil.getCurrentTime());
		usermLogin.setUserName(shiroUserm.getName());
		if(old == null){
			usermLogin.setCreateTime(CalendarUtil.getCurrentTime());
			super.save(usermLogin);
		}else{
			usermLogin.setLastIp(old.getCurrentIp());
			usermLogin.setLastLoginTime(old.getCurrentLoginTime());
			usermLogin.setCreateTime(old.getCreateTime());
			super.updata(usermLogin);
		}
	}

}
