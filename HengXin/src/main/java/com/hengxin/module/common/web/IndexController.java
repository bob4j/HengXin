/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.web</p>
 * <p>文件名：IndexController.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-3-下午1:33:58</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**<p>名称：IndexController.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-3 下午1:33:58
 * @version 1.0.0
 */

@Controller
@RequestMapping("/common")
public class IndexController {

	
	@RequestMapping({ "/index" })
	public String index() {
		return "common/main";
	}

}
