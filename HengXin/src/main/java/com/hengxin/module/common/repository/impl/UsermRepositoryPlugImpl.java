/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.repository.impl</p>
 * <p>文件名：UsermRepositoryPlugImpl.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-25-下午4:57:31</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.repository.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import cn.singno.bob.jpa.base.impl.BasePlugRepository;
import cn.singno.bob.jpa.bean.SQLHelper;

import com.google.common.collect.Lists;
import com.hengxin.module.common.entity.Userm;
import com.hengxin.module.common.model.UsermModel;

/**<p>名称：UsermRepositoryPlugImpl.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-25 下午4:57:31
 * @version 1.0.0
 */
public class UsermRepositoryPlugImpl extends BasePlugRepository<Userm, Long> {

	private static String FIND_LIST = "select o from Userm o, UsermOrganization u where u.organizationId = ?";
	
	
	/**
	 * 动态分页查询
	 * <p>描述：</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param userm
	 * @return
	 */
	public Page<Userm> searchByConditions(UsermModel usermModel, Long organIds, Pageable page){
		if(organIds == null){
			return new PageImpl(Lists.newArrayList());
		}
		SQLHelper helper = new SQLHelper(FIND_LIST).addParam(organIds);
		return super.find(helper.toString(), helper.toParams(), page);
	}

}
