package com.hengxin.module.common.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.ScriptAssert;

import com.hengxin.validated.group.Save;
import com.hengxin.validated.group.Update;

/**<p>名称：UsermModel.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-10 上午11:44:49
 * @version 1.0.0
 */
@ScriptAssert(lang="javascript", script="_this.newPwd == _this.newAgainPwd", alias="_this", message="二次输入的密码不一致", groups={Update.class})
public class UsermModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2520892438080597604L;

	@NotBlank(message="新密码不能为空", groups={Update.class})
	private String newPwd;		//新密码
	
	@NotBlank(message="旧密码不能为空", groups={Update.class})
	private String oldPwd;		//旧密码
	
	@NotBlank(message="再次确认密码不能为空", groups={Update.class})
	private String newAgainPwd;	//再次确认密码
	
	private String name;
	private String account;
	private Integer age;
	private Short sex;
	private String email;
	private Short userState;   //用户状态 0:正常状态 1:冻结状态
	
	@NotNull(message="请选择用户的组织信息", groups={Save.class})
	private Long organizationId;
	
	@NotNull(message="请选择用户的身份信息", groups={Save.class})
	private Long roleId;
	

	/**
	 * @return the newPwd
	 */
	public String getNewPwd() {
		return newPwd;
	}

	/**
	 * @param newPwd the newPwd to set
	 */
	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	/**
	 * @return the oldPwd
	 */
	public String getOldPwd() {
		return oldPwd;
	}

	/**
	 * @param oldPwd the oldPwd to set
	 */
	public void setOldPwd(String oldPwd) {
		this.oldPwd = oldPwd;
	}

	/**
	 * @return the newAgainPwd
	 */
	public String getNewAgainPwd() {
		return newAgainPwd;
	}

	/**
	 * @param newAgainPwd the newAgainPwd to set
	 */
	public void setNewAgainPwd(String newAgainPwd) {
		this.newAgainPwd = newAgainPwd;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * @return the sex
	 */
	public Short getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(Short sex) {
		this.sex = sex;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the userState
	 */
	public Short getUserState() {
		return userState;
	}

	/**
	 * @param userState the userState to set
	 */
	public void setUserState(Short userState) {
		this.userState = userState;
	}

	/**
	 * @return the organizationId
	 */
	public Long getOrganizationId() {
		return organizationId;
	}

	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * @return the roleId
	 */
	public Long getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	
	
}
