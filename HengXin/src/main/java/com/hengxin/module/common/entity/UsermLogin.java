package com.hengxin.module.common.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Usermlogin entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "usermlogin")
public class UsermLogin implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -614472830259661377L;
	private Long userId;
	private String lastIp;
	private String currentIp;
	private String userName;
	private Long lastLoginTime;
	private Long currentLoginTime;
	private Long createTime;

	// Constructors

	/** default constructor */
	public UsermLogin() {
	}

	/** minimal constructor */
	public UsermLogin(Long userId, String currentIp, String userName,
			Long currentLoginTime, Long createTime) {
		this.userId = userId;
		this.currentIp = currentIp;
		this.userName = userName;
		this.currentLoginTime = currentLoginTime;
		this.createTime = createTime;
	}

	/** full constructor */
	public UsermLogin(Long userId, String lastIp, String currentIp,
			String userName, Long lastLoginTime, Long currentLoginTime,
			Long createTime) {
		this.userId = userId;
		this.lastIp = lastIp;
		this.currentIp = currentIp;
		this.userName = userName;
		this.lastLoginTime = lastLoginTime;
		this.currentLoginTime = currentLoginTime;
		this.createTime = createTime;
	}

	// Property accessors
	@Id
	@Column(name = "userId", unique = true, nullable = false)
	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "lastIp")
	public String getLastIp() {
		return this.lastIp;
	}

	public void setLastIp(String lastIp) {
		this.lastIp = lastIp;
	}

	@Column(name = "currentIp", nullable = false)
	public String getCurrentIp() {
		return this.currentIp;
	}

	public void setCurrentIp(String currentIp) {
		this.currentIp = currentIp;
	}

	@Column(name = "userName", nullable = false, length = 30)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "lastLoginTime")
	public Long getLastLoginTime() {
		return this.lastLoginTime;
	}

	public void setLastLoginTime(Long lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	@Column(name = "currentLoginTime", nullable = false)
	public Long getCurrentLoginTime() {
		return this.currentLoginTime;
	}

	public void setCurrentLoginTime(Long currentLoginTime) {
		this.currentLoginTime = currentLoginTime;
	}

	@Column(name = "createTime", nullable = false)
	public Long getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Long createTime) {
		this.createTime = createTime;
	}

}