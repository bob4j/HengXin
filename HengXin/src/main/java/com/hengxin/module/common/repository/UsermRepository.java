/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.repository</p>
 * <p>文件名：UsermRepository.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-9-下午12:09:55</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import cn.singno.bob.jpa.base.BaseRepository;

import com.hengxin.module.common.entity.Userm;
import com.hengxin.module.common.model.UsermModel;

/**<p>名称：UsermRepository.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-9 下午12:09:55
 * @version 1.0.0
 */
public interface UsermRepository extends BaseRepository<Userm, Long> {

	/**
	 * 
	 * <p>描述：通过账号来查询用户</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param account
	 * @return
	 */
	@Query("select o from Userm o where o.account = ?1")
	public Userm findByAccount(String account);
	
	/**
	 * 
	 * <p>描述：修改密码</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param id
	 * @param pwd
	 */
	@Modifying 
	@Query("update Userm  set pwd = ?2 where id = ?1")
	public int updatePwd(Long id, String pwd);
	
	/**
	 * 
	 * <p>描述：根据状态来查询对应的用户</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param id
	 * @param userState
	 * @return
	 */
	@Query("select o from Userm o where o.id = ?1 and o.userState = ?2")
	public Userm findById(Long id, Short userState);
	
	/**
	 * 
	 * <p>描述：查询该账号是否已经使用</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param account
	 * @return
	 */
	@Query("select 1 from Userm o where o.account = ?1")
	public Integer isExist(String account);
	
	
	/**
	 * 动态分页查询
	 * <p>描述：</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param userm
	 * @return
	 */
	public Page<Userm> searchByConditions(UsermModel usermModel, Long organIds, Pageable page);
}
