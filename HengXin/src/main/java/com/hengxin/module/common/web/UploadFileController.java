/**<p>项目名：</p>
 * <p>包名：	com.hengxin.module.common.web</p>
 * <p>文件名：UploadFileController.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-14-下午1:20:43</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.module.common.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.hengxin.constant.ApplicationContext;

import cn.singno.bob.bean.JsonMessage;
import cn.singno.bob.emun.CommonEmun;
import cn.singno.bob.jpa.utils.JsonMessageUtils;
import cn.singno.bob.web.base.BaseController;

/**<p>名称：UploadFileController.java</p>
 * <p>描述：统一文件上传控制器</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-14 下午1:20:43
 * @version 1.0.0
 */
@RequestMapping(ApplicationContext.COMMON_MODULE + "/upload")
@Controller
public class UploadFileController extends BaseController{

	/**
	 * 
	 * <p>描述：文档上传</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param doc
	 * @return
	 */
	@RequestMapping(value = "/doc", method=RequestMethod.POST)
	public JsonMessage uploadDoc(MultipartFile Filedata){
		if(Filedata.isEmpty()){
			System.out.println("xxxxxxxxxxxx");
		}
		System.out.println("ffffffffffffffff");
		return JsonMessageUtils.getJsonMessage(CommonEmun.SUCCESS);
	}
	
}
