/**<p>项目名：</p>
 * <p>包名：	com.hengxin.resolver</p>
 * <p>文件名：MySortHandlerMethodArgumentResolver.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-9-上午9:56:25</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.resolver;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.SortHandlerMethodArgumentResolver;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;

/**<p>名称：MySortHandlerMethodArgumentResolver.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-9 上午9:56:25
 * @version 1.0.0
 */
public class MySortHandlerMethodArgumentResolver extends SortHandlerMethodArgumentResolver{

	// ASC || desc 的参数名称 
	private String sortPropertyParameter;					
	
	/**
	 * @return the sortPropertyParameter
	 */
	public String getSortPropertyParameter() {
		return sortPropertyParameter;
	}

	/**
	 * @param sortPropertyParameter the sortPropertyParameter to set
	 */
	public void setSortPropertyParameter(String sortPropertyParameter) {
		this.sortPropertyParameter = sortPropertyParameter;
	}


	/* (non-Javadoc)
	 * @see org.springframework.data.web.SortHandlerMethodArgumentResolver#resolveArgument(org.springframework.core.MethodParameter, org.springframework.web.method.support.ModelAndViewContainer, org.springframework.web.context.request.NativeWebRequest, org.springframework.web.bind.support.WebDataBinderFactory)
	 */
	@Override
	public Sort resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {
		String[] sortParams = webRequest.getParameterValues(getSortParameter(parameter));
		String sortPropertyParam = webRequest.getParameter(sortPropertyParameter);
		return myParseParameterIntoSort(sortParams, sortPropertyParam);
	}
	
	private Sort myParseParameterIntoSort(String[] parameter, String sortPropertyParameter){
		return ArrayUtils.isNotEmpty(parameter) ? new Sort(Direction.fromStringOrNull(sortPropertyParameter), parameter) : null;
	}
	
	
}
