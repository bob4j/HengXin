/**<p>项目名：</p>
 * <p>包名：	com.hengxin.shiro.exception</p>
 * <p>文件名：CaptchaException.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-12-上午11:27:18</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.shiro.exception;

import org.apache.shiro.authc.AuthenticationException;

/**<p>名称：CaptchaException.java</p>
 * <p>描述：验证码错误异常</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-12 上午11:27:18
 * @version 1.0.0
 */
public class CaptchaException extends AuthenticationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3889380766794117413L;

	
	 /**
     * Creates a new AuthenticationException.
     */
    public CaptchaException() {
        super();
    }

    /**
     * Constructs a new AuthenticationException.
     *
     * @param message the reason for the exception
     */
    public CaptchaException(String message) {
        super(message);
    }

    /**
     * Constructs a new AuthenticationException.
     *
     * @param cause the underlying Throwable that caused this exception to be thrown.
     */
    public CaptchaException(Throwable cause) {
        super(cause);
    }

    /**
     * Constructs a new AuthenticationException.
     *
     * @param message the reason for the exception
     * @param cause   the underlying Throwable that caused this exception to be thrown.
     */
    public CaptchaException(String message, Throwable cause) {
        super(message, cause);
    }
	
}
