package com.hengxin.shiro.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;

import cn.singno.bob.web.utils.IPUtils;

import com.hengxin.bean.ShiroUserm;
import com.hengxin.module.common.service.UsermLoginService;
import com.hengxin.shiro.bean.CaptchaUsernamePasswordToken;

/**
 * <p>名称：CaptchaFormAuthenticationFilter.java</p>
 * <p>描述：验证码表单认证过滤器</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014年9月20日 上午11:45:52
 * @version 1.0.0
 */
public class CaptchaFormAuthenticationFilter extends FormAuthenticationFilter{

	public static final String DEFAULT_CAPTCHA_PARAM = "captcha";
	
	private String captchaParam = DEFAULT_CAPTCHA_PARAM;
	
	private UsermLoginService usermLoginService;
	
	
	
	
	/**
	 * @param usermLoginService the usermLoginService to set
	 */
	public void setUsermLoginService(UsermLoginService usermLoginService) {
		this.usermLoginService = usermLoginService;
	}
	



	/**
	 * 
	 * <p>描述：获取验证码的key</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	public String getCaptchaParam() {
		return captchaParam;
	}
	
	public void setCaptchaParam(String captchaParam) {
		this.captchaParam = captchaParam;
	}




	/**
	 * 
	 * <p>描述：获取验证码</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param request
	 * @return
	 */
	protected String getCaptcha(ServletRequest request) {
		return WebUtils.getCleanParam(request, getCaptchaParam());
	}
	
	/**
	 * 创建一个带有验证码的token对象
	 */
	@Override
	protected AuthenticationToken createToken(ServletRequest request,
			ServletResponse response) {
		String username = getUsername(request);
		String password = getPassword(request);
		String captcha = getCaptcha(request);
		boolean rememberMe = isRememberMe(request);
		String host = getHost(request);
		return new CaptchaUsernamePasswordToken(username, password, rememberMe, host, captcha);
	}
	
	/* (non-Javadoc)
	 * 重写登陆成功后的业务逻辑
	 * @see org.apache.shiro.web.filter.authc.FormAuthenticationFilter#onLoginSuccess(org.apache.shiro.authc.AuthenticationToken, org.apache.shiro.subject.Subject, javax.servlet.ServletRequest, javax.servlet.ServletResponse)
	 */
	@Override
	protected boolean onLoginSuccess(AuthenticationToken token,
			Subject subject, ServletRequest request, ServletResponse response)
			throws Exception {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        saveLog(subject, httpServletRequest);
        
        httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + this.getSuccessUrl());
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.apache.shiro.web.filter.authc.FormAuthenticationFilter#onLoginFailure(org.apache.shiro.authc.AuthenticationToken, org.apache.shiro.authc.AuthenticationException, javax.servlet.ServletRequest, javax.servlet.ServletResponse)
	 */
	@Override 
	protected boolean onLoginFailure(AuthenticationToken token,
			AuthenticationException e, ServletRequest request,
			ServletResponse response) {
		 super.onLoginFailure(token, e, request, response);
		return true;
	}
	
	/**
	 * 
	 * <p>描述：保存登录的日志</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param subject
	 * @param httpServletRequest
	 */
	private void saveLog(Subject subject, HttpServletRequest httpServletRequest){
		ShiroUserm shiroUserm = (ShiroUserm)subject.getPrincipal();
        usermLoginService.save(shiroUserm, IPUtils.getIpAddr(httpServletRequest));
	}
	
	
	
}
