package com.hengxin.shiro.bean;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * <p>名称：CaptchaUsernamePasswordToken.java</p>
 * <p>描述：带有验证码的一个用户token</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014年9月20日 上午11:51:41
 * @version 1.0.0
 */
public class CaptchaUsernamePasswordToken extends UsernamePasswordToken {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2454608083648750362L;

	/**
	 * 验证码
	 */
	private String captcha;
	
	public CaptchaUsernamePasswordToken() {
		super();
	}

	public CaptchaUsernamePasswordToken(String username, String password,
			boolean rememberMe, String host, String captcha) {
		super(username, password, rememberMe, host);
		this.captcha = captcha;
	}
	

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	

}
