package com.hengxin.constant;

import cn.singno.bob.emun.BusinessCode;

/**
 * <p>名称：HengXinBusinessEnum.java</p>
 * <p>描述：异常错误信息</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-3 下午1:31:05
 * @version 1.0.0
 */
public enum HengXinBusinessEnum  implements BusinessCode
{

  //10100 - 10300   通用型异常错误信息
  RECORD_NOT_FIND(10100, "数据不存在或者已经被删除"),
  USERM_NOT_FIND(10101, "用户不存在或者已经被冻结"),
  
  //10400-10600   用户相关
  USERM_PWD_ERROR(10400, "输入的密码不正确"),
  ROLE_INFO_ERROR(10401, "身份信息有误"),
  ROLE_INFO_NOT_FIND(10402, "身份信息不存在"),
  USERM_ACCOUNT_EXIST(10403, "该账户已经被使用了，请换一个账号")
  ;

  
  private Integer code;
  private String message;

  private HengXinBusinessEnum(Integer code, String message)
  {
    this.code = code;
    this.message = message;
  }

  public Integer getCode()
  {
    return this.code;
  }

  public String getMessage()
  {
    return this.message;
  }
}