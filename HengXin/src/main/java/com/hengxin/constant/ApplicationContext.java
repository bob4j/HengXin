package com.hengxin.constant;


/**
 * <p>名称：ApplicationContext.java</p>
 * <p>描述：项目常量类</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-3 下午1:31:44
 * @version 1.0.0
 */
public final class ApplicationContext{

	/**
	 * 通用模块
	 */
	public static final String COMMON_MODULE = "/common";
	
	/**
	 * 合同模块
	 */
	public static final String CONTRACT_MODULE = "/contract";
	
	
	/**
	 * 用户模块
	 */
	public static final String USERM_MODULE = "/userm";
	
	/**
	 * 产品模块
	 */
	public static final String PRODUCT = "/product";
	
	/**
	 * 系统设置模块
	 */
	public static final String SETTING = "/setting";
	
	/**
	 * 删除状态-删除
	 */
	public static final Boolean DEL_STATE_TRUE = Boolean.TRUE;
	

	/**
	 * 删除状态-非删除
	 */
	public static final Boolean DEL_STATE_FALSE = Boolean.FALSE;
	
	
	
	/**
	 * 合同编号前缀
	 */
	public static final String CONTRACT_PREFIX_NO = "HT";
	
}