/**<p>项目名：</p>
 * <p>包名：	com.hengxin.constant</p>
 * <p>文件名：CacheConstant.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-10-上午9:46:26</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.constant;

/**<p>名称：CacheConstant.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-10 上午9:46:26
 * @version 1.0.0
 */
public class CacheConstant {

	/**
	 * 密码重试缓存
	 */
	public static final String PASSWORD_RETRY_CACHE = "passwordRetryCache";
}
