/**<p>项目名：</p>
 * <p>包名：	com.hengxin.constant</p>
 * <p>文件名：StateConstant.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午9:17:20</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.constant;

/**<p>名称：StateConstant.java</p>
 * <p>描述：状态常量类</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午9:17:20
 * @version 1.0.0
 */
public class StateConstant {

	/**
	 * 编号-合同类型
	 */
	public static final  Short NOFACTORY_USEFOR_CONTRACT = 0; 
	
	/**
	 * 合同的状态 - 未报
	 */
	public static final Short CONTRACT_STATE_NOT_REPORTED = 1;
	
	/**
	 * 合同的状态 - 已报
	 */
	public static final Short CONTRACT_STATE_REPORTED = 2;
	
	/**
	 * 合同的状态 - 未批
	 */
	public static final Short CONTRACT_STATE_NOT_APPROVED = 3;
	
	/**
	 * 合同的状态 - 资料上交 
	 */
	public static final Short CONTRACT_STATE_DATA_HAND = 4;
	
	/**
	 * 合同的状态 - 工程返利
	 */
	public static final Short CONTRACT_STATE_PROJECT_REBATE = 5;
	
	/**
	 * 用户状态-正常
	 */
	public static final Short USERM_STATE_RIGHT = 0;
	
	/**
	 * 用户状态-冻结
	 */
	public static final Short USERM_STATE_FROZEN = 1;
	
	/**
	 * 启用状态
	 */
	public static final Short STATE_0 = 0;
	
	/**
	 * 停用状态
	 */
	public static final Short STATE_1 = 1;
	
}
