/**<p>项目名：</p>
 * <p>包名：	com.hengxin.bean</p>
 * <p>文件名：DeleteModel.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午2:24:07</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.bean;

import java.io.Serializable;
import java.util.List;

/**<p>名称：DeleteModel.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午2:24:07
 * @version 1.0.0
 */
public class DeleteModel<T> implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -709157024702790747L;
	private List<T> deleteModel;

	/**
	 * @return the deleteModel
	 */
	public List<T> getDeleteModel() {
		return deleteModel;
	}

	/**
	 * @param deleteModel the deleteModel to set
	 */
	public void setDeleteModel(List<T> deleteModel) {
		this.deleteModel = deleteModel;
	}
	
}
