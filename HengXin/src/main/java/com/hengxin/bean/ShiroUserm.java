/**<p>项目名：</p>
 * <p>包名：	com.hengxin.bean</p>
 * <p>文件名：ShiroUserm.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-10-上午10:24:25</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.bean;

import java.io.Serializable;

import com.hengxin.module.common.entity.Userm;

/**<p>名称：ShiroUserm.java</p>
 * <p>描述：</p>
 * <pre>
 *    Shiro用户对象
 * </pre>
 * @author 鲍建明
 * @date 2014-11-10 上午10:24:25
 * @version 1.0.0
 */
public class ShiroUserm implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8180813034001292772L;

	private Long id;
	
	private String name;
	
	private String account;
	
	private Integer age;
	
	private Short sex;
	
	private Short userState;
	
	private String email;
	
	private String salt;

	private Long organId;
	
	
	
	/**
	 * <p>构造器：</p>
	 * <pre>
	 *    
	 * </pre>
	 */
	public ShiroUserm(Userm userm) {
		super();
		this.id = userm.getId();
		this.name = userm.getName();
		this.age = userm.getAge();
		this.userState = userm.getUserState();
		this.sex = userm.getSex();
		this.email = userm.getEmail();
		this.account = userm.getAccount();
		this.salt = userm.getSalt();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public Integer getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(Integer age) {
		this.age = age;
	}

	/**
	 * @return the sex
	 */
	public Short getSex() {
		return sex;
	}

	/**
	 * @param sex the sex to set
	 */
	public void setSex(Short sex) {
		this.sex = sex;
	}

	/**
	 * @return the userState
	 */
	public Short getUserState() {
		return userState;
	}

	/**
	 * @param userState the userState to set
	 */
	public void setUserState(Short userState) {
		this.userState = userState;
	}
	
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}
	
	

	/**
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}

	/**
	 * @param salt the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	/**
	 * @return the organId
	 */
	public Long getOrganId() {
		return organId;
	}

	/**
	 * @param organId the organId to set
	 */
	public void setOrganId(Long organId) {
		this.organId = organId;
	}


	
}
