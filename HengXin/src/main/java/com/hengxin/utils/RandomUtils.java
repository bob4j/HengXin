/**<p>项目名：</p>
 * <p>包名：	com.hengxin.utils</p>
 * <p>文件名：RandomUtils.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-5-上午10:19:33</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.utils;

import java.util.Random;

import org.apache.commons.lang3.StringUtils;

import cn.singno.bob.common.utils.CalendarUtil;

/**<p>名称：RandomUtils.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-5 上午10:19:33
 * @version 1.0.0
 */
public class RandomUtils {

	/**
	 * 
	 * <p>描述：获取下一个编号</p>
	 * <pre>
	 *    HT20141105-001
	 * </pre>
	 * @param prefix
	 * @param No
	 * @return
	 */
	public static String getNextNo(String prefix, String No){
		if(StringUtils.isBlank(No)){
			return prefix + CalendarUtil.toString(CalendarUtil.FMT_YMD) + "-0001";
		}
		String[] strs = No.split("-");
		Integer i = Integer.valueOf(strs[1]) + 1;
		return  strs[0] + "-" + String.format("%04d", i);
	}
	
	/**
	 * 
	 * <p>描述：随机获取4位数的随机数</p>
	 * <pre>
	 *    
	 * </pre>
	 * @param wei
	 * @return
	 */
	public static String getRandom(){
		Random r = new Random();
		return String.format("%04d", r.nextInt(9999));
	}
	
}
