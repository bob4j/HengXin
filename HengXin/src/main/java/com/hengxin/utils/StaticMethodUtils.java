/**<p>项目名：</p>
 * <p>包名：	com.hengxin.utils</p>
 * <p>文件名：StaticMethodUtils.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-5-上午10:27:34</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.utils;

import cn.singno.bob.web.utils.SpringContextHelper;

import com.hengxin.module.common.entity.Nofactory;
import com.hengxin.module.common.service.NofactoryService;

/**<p>名称：StaticMethodUtils.java</p>
 * <p>描述：用于页面中静态的方法</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-5 上午10:27:34
 * @version 1.0.0
 */
public class StaticMethodUtils {

	private static NofactoryService NofactoryService = SpringContextHelper.getBean(NofactoryService.class);
	
	/**
	 * 
	 * <p>描述：获取合同编号</p>
	 * <pre>
	 *    
	 * </pre>
	 * @return
	 */
	public synchronized static String getContractNo(){
		Nofactory nofactory = NofactoryService.buildContractNo();
		return nofactory.getNumber();
	}
	
}
