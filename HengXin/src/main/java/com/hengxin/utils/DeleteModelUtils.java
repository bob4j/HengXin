/**<p>项目名：</p>
 * <p>包名：	com.hengxin.utils</p>
 * <p>文件名：DeleteModelUtils.java</p>
 * <p>版本信息：</p>
 * <p>日期：2014-11-4-下午2:41:08</p>
 * Copyright (c) 2014singno.bob公司-版权所有
 */
package com.hengxin.utils;

import java.util.List;

import com.alibaba.fastjson.JSON;

/**<p>名称：DeleteModelUtils.java</p>
 * <p>描述：</p>
 * <pre>
 *    
 * </pre>
 * @author 鲍建明
 * @date 2014-11-4 下午2:41:08
 * @version 1.0.0
 */
public class DeleteModelUtils<T> {

	/**
	 * 
	 * <p>描述：</p>
	 * <pre>
	 * 	List<JSONObject> 转换成对应的List<T>    
	 * </pre>
	 * @param delteJson
	 * @param clazz
	 * @return
	 */
	public static <T> List<T> toDeleteModel(List<?> delteJson, Class<T> clazz){
		String str = JSON.toJSONString(delteJson);
		return  JSON.parseArray(str, clazz);
		/*List<T> result = Lists.newArrayList();
		for (Object obj : delteJson) {
			if(obj instanceof JSONObject){
				JSONObject o = (JSONObject)obj;
				result.add(JSON.parseObject(o.toJSONString(), clazz));
			}
		}
		return result;*/
	}
	
}
