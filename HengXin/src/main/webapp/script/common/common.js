
/**
 * addTab("合同保存", "${ctx}/product/load");
 * @param subtitle
 * @param url
 */
function addTab(subtitle, url) {
	if (!$('#tabs').tabs('exists', subtitle)) {
		$('#tabs').tabs('add', {
			title : subtitle,
			// content: createFrame(url),
			href : url,
			closable : true,
			width : $('#mainPanle').width() - 10,
			height : $('#mainPanle').height() - 26
		});
	} else {
		$('#tabs').tabs('select', subtitle);
	}
}

/*//产品选择
function addChange(productTable){
	changeAll = $('#productDataGrid').datagrid('getChecked');
	var salePrices = 0;
	$.each(changeAll, function (i, o){
		if(!isNaN(o.salePrice)){
			salePrices += o.salePrice;
		}
	});
	$('#' + productTable).combogrid().combogrid("grid").datagrid("loadData", changeAll);
	$('#totalMoney').val(salePrices.toFixed(2));
	$('#productWin').window('close');
}*/

/**
 * 插入iframe
 * @param subtitle
 * @param url
 */
function addTab4Framm(subtitle, url) {
	if (!$('#tabs').tabs('exists', subtitle)) {
		$('#tabs').tabs('add', {
			title : subtitle,
			 content: createFrame(url),
		//	href : url,
			closable : true,
			width : $('#mainPanle').width() - 10,
			height : $('#mainPanle').height() - 26
		});
	} else {
		$('#tabs').tabs('select', subtitle);
	}
}


//格式化时间
function formatDate(value){
	if(value){
		return moment(value).format("YYYY-MM-DD");
	}
}


//关闭指定Tab  
function closeTab(title) {  
    if ($('#tabs').tabs('exists', title)) {  
        $('#tabs').tabs('close', title);  
    }  
}

//刷新指定Tab的内容  
function refreshTab(title){  
	if ($('#tabs').tabs('exists', title)){  
        var currTab = $('#tabs').tabs('getTab', title),  
            iframe = $(currTab.panel('options').href),  
            href =  iframe.attr('src');  
        $('#tabs').tabs('update', {tab: currTab, options: {href: href, closable: true}});  
    }  
}  



/**
 * 打开一个新的窗口
 * 	myWindow('saveWin', '产品新增', '${ctx}/product/load', 800, 300);
 * @param target 
 * @param title
 * @param url
 * @param width
 * @param height
 */
function myWindow(target, title, url, width, height){
  	$('#' + target).window({
  		width: width,
  		height: height,
  		title : title,
  		closed: false,
  		modal: true,
  		top: $(document).scrollTop() + ($(window).height()-300) * 0.5,
  		href : url
  	});
}



function createFrame(url) {
    var s = '<iframe name="mainFrame" scrolling="auto" frameborder="0"  src="' + url + '" style="width:100%;height:100%;"></iframe>';
    return s;
} 

