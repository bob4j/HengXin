/** 调用方式：$.ajaxUtils({
	 * 			url : '',
	 * 			data : ''
	 * 			})
	 * 
	 * 
	 * 
	 * 参数说明:
	 *  type : 'post',  默认请求类型post
		data : '', 默认请求参数为空字符串
		url : '', 请求路径没有 则会报throw new Error("url does not exist");
		timeout : 3000, 默认请求超时时间为3000
		async : false, 默认请求异步
		dataType : 'json' 默认请求参数类型json
		success : 成功直接调用 和下面2个方法冲突。2选一调用
		successMethod : 自定义code成功后方法 默认alert("成功!")
		errorMethod : 自定义code失败后方法 默认alert(data.message);
		ajaxBefore : //发送请求前调用
	 * 
	 * 
	 * 
	 */
(function ($){
	$.ajaxUtils = function (options){
		var _this = this;
		
		//初始化
		_this.initMethod = function (){			//不能使用方法名为init的。这样会和jquery方法本身有冲突
			var opt = $.extend($.ajaxUtils.defaultOptions, options);
			_this.opt = opt;
			_this.verify();
			$.messager.progress({
				title:'请稍后',
				msg:'数据请求中...',
				interval : 3000
			});
			_this.send(opt);
		};
		
		//校验参数
		_this.verify = function (){	
			if($.isEmptyObject(options.data)){
				options.data = '';
			}
			if(typeof(options.url) == 'undefined' || options.url == ''){
				throw new Error("url does not exist");
			}
		};

		//发送请求
		_this.send = function (opt){
			$.ajax({
				type : opt.type,
				url : opt.url,
				data : opt.data,
				dataType : opt.dataType,
				timeout : opt.timeout,
				async : opt.async,
				contentType: opt.contentType,
				success : function (data, textStatus){
					$.messager.progress('close');
					
					if( $.isFunction(opt.success) ){
						opt.success(data, textStatus);
					}else if($.isFunction(_this.suces) ){
						_this.suces(data, textStatus);
					}else {
						throw new Error("no method can execute");
					}
				},
				error : function (XMLHttpRequest, textStatus, errorThrown){
					$.messager.progress('close');
					if($.isFunction(opt.errorMethod)){
						opt.errorMethod(XMLHttpRequest, textStatus, errorThrown);
					}else{
						$.messager.alert('错误提示','网络故障','error');
					}
				},
				beforeSend : function (){
					if($.isFunction(options.ajaxBefore)){
						options.ajaxBefore();			//发送请求前调用
					}
				}
				
			});
		};
		
		_this.suces = function (data, textStatus){				//自定义成功方法执行
			if(data.code == 200){
				if($.isFunction(_this.opt.successMethod)){		//自定义code成功后方法
					_this.opt.successMethod(data);
				}else{
					$.messager.alert('提示','成功!');
				}
			}else {
				if($.isFunction(_this.opt.errorMethod)){			//自定义code失败后方法
					_this.opt.errorMethod(data);
				}else{
					$.messager.alert('错误提示',data.message,'error');
				}
			}
		};
		
		
		_this.initMethod();
	}
	
	//默认行为
	$.ajaxUtils.defaultOptions = {
			type : 'post',
			data : '',
			url : '',
			timeout : 3000,
			async : false,
			dataType : 'json'
			/*successMethod : 
			errorMethod : 
			ajaxBefore : */
	}
	
	
})(jQuery);