$(function (){

	var changeAll = '${products}';


	var productTable = $('#productTable').combogrid({
			panelWidth:800,
			value:'已选择产品列表',
			columns:[[
				{field:'id',title:'ID',hidden:true,width:60},	
				{field:'number',title:'产品编号',width:60},
				{field:'standard',title:'产品规格',width:120},
				{field:'voltage',title:'电压',width:100},
				{field:'unit',title:'单位',width:100},
				{field:'stock',title:'产品库存',width:100},
				{field:'stockPrice',title:'进货单价',width:100},
				{field:'salePrice',title:'销售价格',width:100},
				{field:'stockNum',title:'进货数量',width:100}
			]]
		});


	$('#productSeach').on('click', function (){
		$('#productWin').window('open');
		$('#productPanle').panel({
			href : '${ctx }/product/list?flag=productChange'
		});		
	});

	

	$('#saveBtn').on('click', function (){
		var contract = $('#contractForm').serializeJSON();
		contract.product = changeAll;
		$.ajaxUtils({
			data : JSON.stringify(contract),
			url : '${ctx}/contract/saveOrUpdate',
			contentType: "application/json; charset=utf-8",
			async : true
		});	
		
	});
	
	if(changeAll != ''){
		changeAll = $.parseJSON(changeAll);
		productTable.combogrid("grid").datagrid("loadData", changeAll);
	}
	
	
	//产品选择
	function addChange(){
		changeAll = $('#productDataGrid').datagrid('getChecked');
		var salePrices = 0;
		$.each(changeAll, function (i, o){
			if(!isNaN(o.salePrice)){
				salePrices += o.salePrice;
			}
		});
		productTable.combogrid("grid").datagrid("loadData", changeAll);
		$('#totalMoney').val(salePrices.toFixed(2));
		$('#productWin').window('close');
	}
	
	

});



		
