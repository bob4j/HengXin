<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="leftMenu" class="easyui-accordion" style="width:100%;height:100%;">
    <div title="合同相关"  selected="true" style="overflow:auto;padding:5px;">
  	  		<ul class="easyui-tree">
                <li>
                    <a href="${ctx }/contract/list">合同管理</a>
                </li>
                 <li>
                    <a href="${ctx }/contract/template/list">合同模板管理</a>
                </li>
                 <li>
                    <a href="${ctx }/product/list">产品管理</a>
                </li>
            </ul>
    </div>
    <div title="用户相关"  style="padding:5px;">
       <ul class="easyui-tree" >
               
                <li>
                   <span>权限管理</span>
                   <ul>
                       <li>
                           <span>权限列表</span>
                       </li>
                       <li>
                           <span>权限新增</span>
                       </li>
                   </ul>
               </li>
               
               
           </ul>
    </div>
    <div title="系统设置"  style="padding:5px;">
      		<ul class="easyui-tree">
               <li>
                   <span>日志管理</span>
                   <ul>
                       <li>
                           <span>日志列表</span>
                       </li>
                   </ul>
               </li>
               <li>
               		<a href="${ctx }/setting/organization/list">组织设置</a>
               </li>
               <li>
               		<a href="${ctx }/setting/role/list">身份设置</a>
               </li>
               <li>
               		<a href="${ctx }/setting/menu/list">菜单设置</a>
               </li>
               <li>
                    <a href="${ctx }/userm/list">用户管理</a>
               </li>
           </ul>
    </div>
   
</div>
<script type="text/javascript">

$(function (){
 	 $('.easyui-accordion li a').click(function () {
               var tabTitle = $(this).text();
               var url = $(this).attr("href");
              
               addTab(tabTitle, url);
               $('.easyui-accordion li div').removeClass("selected");
               $(this).parent().addClass("selected");
               return false;
           }).hover(function () {
               $(this).parent().addClass("hover");
           }, function () {
               $(this).parent().removeClass("hover");
           });
 

});






    
        //绑定右键菜单事件
    /*    function tabCloseEven() {
               //关闭当前
        $('#mm-tabclose').click(function () {
                   var currtab_title = $('#mm').data("currtab");
                   $('#tabs').tabs('close', currtab_title);
               })
               //全部关闭
        $('#mm-tabcloseall').click(function () {
                   $('.tabs-inner span').each(function (i, n) {
                       var t = $(n).text();
                      $('#tabs').tabs('close', t);
                   });
               });
 
               //关闭除当前之外的TAB
               $('#mm-tabcloseother').click(function () {
                   var currtab_title = $('#mm').data("currtab");
                   $('.tabs-inner span').each(function (i, n) {
                       var t = $(n).text();
                       if (t != currtab_title)
                           $('#tabs').tabs('close', t);
                   });
               });
               //关闭当前右侧的TAB
               $('#mm-tabcloseright').click(function () {
                   var nextall = $('.tabs-selected').nextAll();
                   if (nextall.length == 0) {
                      //msgShow('系统提示','后边没有啦~~','error');
                       alert('后边没有啦~~');
                       return false;
                   }
                   nextall.each(function (i, n) {
                       var t = $('a:eq(0) span', $(n)).text();
                       $('#tabs').tabs('close', t);
                   });
                   return false;
              });
               //关闭当前左侧的TAB
               $('#mm-tabcloseleft').click(function () {
                   var prevall = $('.tabs-selected').prevAll();
                   if (prevall.length == 0) {
                       alert('到头了，前边没有啦~~');
                       return false;
                   }
                   prevall.each(function (i, n) {
                       var t = $('a:eq(0) span', $(n)).text();
                       $('#tabs').tabs('close', t);
                   });
                   return false;
               });
 
               //退出
               $("#mm-exit").click(function () {
                   $('#mm').menu('hide');
 
               })
           }
     */
  

</script>
