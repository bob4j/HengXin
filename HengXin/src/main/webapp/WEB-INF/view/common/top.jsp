<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
    <%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>  

<style>
.top{
	overflow: hidden; 
	height: 30px; 
	background: url(${ctx}/images/layout-browser-hd-bg.gif) #7f99be repeat-x center 50%;  
	line-height: 20px;
	color: #fff; 
	font-family: Verdana, 微软雅黑,黑体
}
.top a{
	color: #fff; 
	font-family: Verdana, 微软雅黑,黑体
}

</style>

	<div class="top">
		<span style="float:right; padding-right:20px;" class="head">
			<shiro:guest>
			    <a href="${ctx }/common/login">返回登录</a>
			</shiro:guest>
			<shiro:user>
				欢迎回来:<shiro:principal property="name" />
				<a href="javascript:;" id="editpass">修改密码</a>
				<a href="${ctx }/common/loginout" >安全退出</a>
			</shiro:user>
		</span>
    	 <span style="padding-left:10px; font-size: 16px; float: left;">
    	 	<img src="images/blocks.gif" width="20" height="20" align="absmiddle" />恒信有限公司
    	 </span>
    	 
    	 	<!-- 修改密码窗口 -->
    	    <div id="editpassWin" class="easyui-window" data-options="title:'My Window',iconCls:'icon-save',closed:true,hidden:true" style="width: 330px; height: 200px; padding: 5px;">
				<div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'center',border:false" style="padding:10px;background:#fff;border:1px solid #ccc;">
						<form:form action="#" method="post" id="editpassForm" >
							<table cellpadding=3>
								<tr>
			                        <td>旧密码：</td>
			                        <td><input  type="password" name="oldPwd" class="txt01" /></td>
			                    </tr>
			                    <tr>
			                        <td>新密码：</td>
			                        <td><input  type="password" name="newPwd" class="txt01" /></td>
			                    </tr>
			                    <tr>
			                        <td>确认密码：</td>
			                        <td><input  type="password" name="newAgainPwd" class="txt01" /></td>
			                    </tr>
			                </table>
		                </form:form>
					</div>
					<div data-options="region:'south',border:false" style="text-align:right;padding:5px 0;">
						<a class="easyui-linkbutton" id="editpassBtn" data-options="iconCls:'icon-ok'" href="javascript:void(0)" >确定</a>
						<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" href="javascript:void(0)" onclick="javascript:$('#editpassWin').window('close');">取消</a>
					</div>
				</div>
			</div>
    	 
    	 
   </div> 	 
   
<script type="text/javascript">

$(function (){
	
	//密码修改窗口
	$('#editpass').click(function() {
       $('#editpassWin').window('open');
    });
    
    
	//修改密码功能
	$('#editpassBtn').on('click', function (){
		$.ajaxUtils({
			data : $('#editpassForm').serialize(),
			url : '${ctx}/userm/updatePwd',
			successMethod : function (data){
				$.messager.alert('提示','修改密码成功!');
				$('#editpassWin').window('close');
			}
		});
	});
	
	
	
    
   
	

});




</script>   
   
   