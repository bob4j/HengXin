<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/include/tagLib.jsp" %>

<html>
<head>
	<%@include file="/WEB-INF/include/easyUi.jsp" %>
</head>
<body class="easyui-layout">
	<!-- 头部  -->
	<div region="north" split="true" border="false" >
		<%@include file="/WEB-INF/view/common/top.jsp" %>
	</div>		

	<!-- 中间最左 -->
	<div region="west" split="true" title="导航菜单" style="width:200px;">
		<%@include file="/WEB-INF/view/common/left.jsp" %>
	</div>	
	
	<!-- 中间 -->					
	<div region="center" title="欢迎使用" style="padding:5px;background:#eee;">
		<div class="easyui-tabs" id="tabs"  >
		 	<%-- <%@include file="/WEB-INF/view/contract/list.jsp" %> --%>
		 </div>
	</div>				

	<!-- 底部 -->
	<div region="south"  split="true" style="height: 30px; background: #D2E0F2;">
		<%@include file="/WEB-INF/view/common/footer.jsp" %>
	</div>		


</body>
</html>
