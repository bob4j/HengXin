<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%--  新增和修改弹出框 --%>
<link rel="stylesheet" href="${ctx }/css/common.css" type="text/css"></link>
	
		 <form:form action="" method="post" id="productForm">
			<input type="hidden" value="${product.id }" name="id">
			<table  class="table">
				<thead>
					<tr style="height: 50px;">
						<th colspan="4" >
							<h4>${product.id == null ? '产品新增' : '产品修改' }</h4>
						</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td><label>产品编号:</label></td>
						<td><input type="text" name="number" value="${product.number }" /></td>
						<td><label>产品规格:</label></td>
						<td><input type="text" name="standard" value="${product.standard }" /></td>
					</tr>	
				
					<tr>
						<td><label>电压:</label></td>
						<td><input type="text" name="voltage" value="${product.voltage }" /></td>
						<td><label>单位:</label></td>
						<td><input type="text" name="unit" value="${product.unit }" /></td>
					</tr>	
					<tr>
						<td><label>进货单价:</label></td>
						<td><input type="text" name="stockPrice" value="${product.stockPrice }" /></td>
						<td><label>销售单价:</label></td>
						<td><input type="text" name="salePrice" value="${product.salePrice }" /></td>
					</tr>
					<tr>
						<td><label>库存:</label></td>
						<td><input type="text" name="stock" value="${product.stock }" /></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			
			</table>
			<div id="dlg-buttons"  class="dialog-button" style="float: inherit;">
				<a href="javascript:;" class="easyui-linkbutton" id="btnSave">${product.id == null ? '保存' : '修改' }</a>
				<a href="#" class="easyui-linkbutton" onclick="javascript:$('#saveWin').window('close')">关闭</a>
			</div>
		</form:form>
		
<script type="text/javascript">
$(function (){
	$('#btnSave').on('click', function (){
		$.ajaxUtils({
			data : $('#productForm').serialize(),
			url : '${ctx}/product/saveOrUpdate',
			successMethod : function (data){
				$('#saveWin').window('close');
				$('#productDataGrid').datagrid('reload');
			}
		});
	});
});				
</script>		
