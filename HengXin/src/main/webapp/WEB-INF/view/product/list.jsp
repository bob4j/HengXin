<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>



    <table class="easyui-datagrid"  id="productDataGrid"
			data-options="url:'${ctx }/product/list/data',fitColumns:true,nowrap:false,rownumbers:true,showFooter:true,pagination:true,
			loadMsg:'正在加载产品信息列表，请稍后...',toolbar:'#productTb',checkbox:true,method:'get',onDblClickRow:productEditView,pageSize:20">
		<thead>
			<tr>
				<th data-options="field:'id',hidden:true"></th>
				<th data-options="field:'ck',width:80,checkbox:true"></th>
				<th data-options="field:'number',width:80">产品编号</th>
				<th data-options="field:'standard',width:120">产品规格</th>
				<th data-options="field:'voltage',width:80,align:'right'">电压</th>
				<th data-options="field:'salePrice',width:80,align:'right'">售价</th>
				<th data-options="field:'stockPrice',width:250">进货价</th>
				<th data-options="field:'unit',width:60,align:'center'">单位</th> 
				<c:if test="${flag eq 'productChange' }">
				 	<!-- <th field="stockNum" width="80" align="center" editor="numberbox">进货数量</th> -->
					 <th data-options="field:'stockNum',width:60,align:'center',editor:{type:'text'},formatter: function (){return 1;}">进货数量</th> 
				</c:if>
			</tr>
		</thead>
	</table>
	<div id="productTb" style="height:auto">  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onClick="productAddView()">添加</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onClick="productDeleteAll()">删除</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onClick="productReload()">刷新</a>  
    </div> 
	
	
	<div id="saveWin">
	</div> 
	

<jsp:include page="/WEB-INF/include/setup_ajax.jsp"></jsp:include>	
<script type="text/javascript">

  function productAddView(){
  	myWindow('saveWin', '产品新增', '${ctx}/product/load', 800, 300);
  }
  
  function productEditView(rowIndex, rowData){
  		myWindow('saveWin', '产品修改', '${ctx}/product/load?id=' + rowData.id, 800, 300);
  }
  
  
  function productDeleteAll(){
  	var json = $('#productDataGrid').datagrid('getChecked');
  	$.ajaxUtils({
		data : JSON.stringify(json),
		url : '${ctx}/product/deleteAll',
		contentType: "application/json; charset=utf-8",
		successMethod : function (data){
			$.messager.alert('提示','成功!');
			$('#productDataGrid').datagrid('reload');
		}
	}); 
  }

 function productReload(){
 	$('#productDataGrid').datagrid('reload');
 }

	


 
</script>	
