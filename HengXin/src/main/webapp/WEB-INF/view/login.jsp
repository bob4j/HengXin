<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/include/tagLib.jsp"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="description" content="">
<title>登录</title>
<!--为了让IE支持HTML5元素，做如下操作：-->
<!--[if IE]> 
    <script type="text/javascript"> 
    document.createElement("section"); 
    document.createElement("header"); 
    document.createElement("footer"); 
    </script> 
    <![endif]-->
    
<link rel="stylesheet" href="${ctx }/css/login.css" type="text/css"></link>
<script type="text/javascript" src="${ctx }/script/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
$(function (){
	$('#captcha').on('click', function (){
		$('#captcha').attr('src', '${ctx }/getCaptcha?random' + Math.random());
	});
});
</script>
</head>

<body>
	<div class="wrap">
		<form:form action="${ctx }/common/submitLogin" method="post">
			<section class="loginForm">
				<header>
					<h1>登录</h1>
				</header>
				<div class="loginForm_content">
					<fieldset>
						<div class="inputWrap">
							<input type="text" name="account" placeholder="邮箱/会员帐号/手机号"
								autofocus required>
						</div>
						<div class="inputWrap">
							<input type="password" name="pwd" placeholder="请输入密码"
								required>
						</div>
						<div class="inputWrap">
							<input type="text" name="captcha"  style="width: 60%"
								placeholder="请输入验证码" required> 
								<img src="${ctx }/getCaptcha" id="captcha" width="100px;" height="35px;" style="float: right;text-align: inherit;cursor: pointer;">
						</div>
					</fieldset>
					<fieldset>
						<input type="checkbox" checked="checked" name="rememberMe"><span>下次自动登录</span>
						
						<c:choose>
							<c:when test="${fn:contains(shiroLoginFailure, 'CaptchaException') }"><span style="color: red;">验证码不一致</span></c:when>
							<c:when test="${fn:contains(shiroLoginFailure, 'IncorrectCredentialsException') }"><span style="color: red;">密码不正确</span></c:when>
							<c:when test="${fn:contains(shiroLoginFailure, 'UnknownAccountException') }"><span style="color: red;">账号不存在</span></c:when>
							<c:when test="${fn:contains(shiroLoginFailure, 'LockedAccountException') }"><span style="color: red;">账号处于冻结状态</span></c:when>
							<c:when test="${fn:contains(shiroLoginFailure, 'ExcessiveAttemptsException') }"><span style="color: red;">密码输入次数过多,请稍后再试</span></c:when>
						</c:choose>
						
					</fieldset>
					<fieldset>
						<input type="submit" value="登录"><a href="#">忘记密码？</a> <a
							href="#">注册</a>
					</fieldset>
				</div>
			</section>
		</form:form>
	</div>
</body>
</html> 
