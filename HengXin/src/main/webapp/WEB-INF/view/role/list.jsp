<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>



    <table class="easyui-datagrid"  id="role_DataGrid"
			data-options="url:'${ctx }/setting/role/list/data',fitColumns:true,nowrap:false,rownumbers:true,showFooter:true,pagination:true,
			loadMsg:'正在加载身份信息列表，请稍后...',toolbar:'#role_Tb',checkbox:true,method:'get',onDblClickRow:role_EditView,pageSize:20">
		<thead>
			<tr>
				<th data-options="field:'id',hidden:true"></th>
				<th data-options="field:'ck',width:80,checkbox:true"></th>
				<th data-options="field:'name',width:80">名称</th>
				<th data-options="field:'functionMeta',width:120">职能描述</th>
				<th data-options="field:'mobile',width:80,align:'right'">联系电话</th>
				<th data-options="field:'telphone',width:80,align:'right'">办公座机</th>
				<th data-options="field:'cteateTime',width:250">创建时间</th>
				<th data-options="field:'state',width:60,align:'center'">使用状态</th> 
			</tr>
		</thead>
	</table>
	<div id="role_Tb" style="height:auto">  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onClick="role_AddView()">添加</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onClick="role_DeleteAll()">删除</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onClick="role_Reload()">刷新</a>  
    </div> 
	
	
	<div id="role_saveWin">
	</div> 
	

<jsp:include page="/WEB-INF/include/setup_ajax.jsp"></jsp:include>	
<script type="text/javascript">

  function role_AddView(){
  	myWindow('role_saveWin', '身份新增', '${ctx}/setting/role/load', 800, 300);
  }
  
  function role_EditView(rowIndex, rowData){
  		myWindow('role_saveWin', '身份修改', '${ctx}/setting/role/load?id=' + rowData.id, 800, 300);
  }
  
  
  function role_DeleteAll(){
  	var json = $('#role_DataGrid').datagrid('getChecked');
  	$.ajaxUtils({
		data : JSON.stringify(json),
		url : '${ctx}/setting/role/deleteAll',
		contentType: "application/json; charset=utf-8",
		successMethod : function (data){
			$.messager.alert('提示','成功!');
			$('#role_DataGrid').datagrid('reload');
		}
	}); 
  }

 function role_Reload(){
 	$('#role_DataGrid').datagrid('reload');
 }

	


 
</script>	
