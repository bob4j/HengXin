<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%--  新增和修改弹出框 --%>
<link rel="stylesheet" href="${ctx }/css/common.css" type="text/css"></link>
	
		 <form:form action="" method="post" id="role_Form">
			<input type="hidden" value="${role.id }" name="id">
			<table  class="table">
				<thead>
					<tr style="height: 50px;">
						<th colspan="4" >
							<h4>${role.id == null ? '身份新增' : '身份修改' }</h4>
						</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td><label>身份名称:</label></td>
						<td><input type="text" name="name" value="${role.name }" /></td>
						<td><label>状态:</label></td>
						<td>
							<select name="state">
								<option value="0" ${role.state == 0 ? 'selected' : ''} >启用</option>
								<option value="1" ${role.state == 1 ? 'selected' : ''}>不启用</option>
							</select>
						</td>
					</tr>	
					<tr>
						<td><label>授权:</label></td>
						<td colspan="3">
							<input id="role_menu_tree" class="easyui-combotree" multiple  data-options="url:'${ctx }/setting/menu/combotree',method: 'get',idField:'id',textField:'text',onLoadSuccess:load_role_Menu" style="width:80%;">
						</td>
					</tr>
					
						
					<tr>
						<td><label>职能说明:</label></td>
						<td colspan="3"><textarea style="width: 80%; height:100%; margin: 0 auto;" name="functionMeta">${role.functionMeta }</textarea></td>
					</tr>	
				</tbody>
			
			</table>
			<div id="dlg-buttons"  class="dialog-button" style="float: inherit;">
				<a href="javascript:;" class="easyui-linkbutton" id="role_btnSave">${role.id == null ? '保存' : '修改' }</a>
				<a href="#" class="easyui-linkbutton" onclick="javascript:$('#role_saveWin').window('close')">关闭</a>
			</div>
		</form:form>
		
<script type="text/javascript">

var menuIds ${role.menuIds != null ? '=' : ''} ${role.menuIds};
$(function (){
	$('#role_btnSave').on('click', function (){
		var menuIds = $('#role_menu_tree').combotree('getValues');
		console.log(menuIds);
		var role_form = $('#role_Form').serializeJSON();
		role_form.menuIds = menuIds;
		 $.ajaxUtils({
			data : JSON.stringify(role_form),
			url : '${ctx}/setting/role/saveOrUpdate',
			contentType: "application/json; charset=utf-8",
			successMethod : function (data){
				$('#role_saveWin').window('close');
				$('#role_DataGrid').datagrid('reload');
			}
		}); 
	});
	
	
	
	
});	

function load_role_Menu(){
	if(menuIds){
		$('#role_menu_tree').combotree('setValues', menuIds);
	}
}
			
</script>		
