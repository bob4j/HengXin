<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
   <%@taglib prefix="hx" uri="/WEB-INF/tld/hx.tld" %>  
<%--  新增和修改弹出框 --%>
<link rel="stylesheet" href="${ctx }/css/common.css" type="text/css"></link>
	
		 <form:form action="" method="post" id="organization_form">
			<input type="hidden" value="${organization.id }" name="id">
			<input type="hidden" value="" name="parentId">
			<table  class="table">
				<thead>
					<tr style="height: 50px;">
						<th colspan="4" >
							<h4>${organization.id == null ? '组织新增' : '组织修改' }</h4>
						</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td><label>机构父级:</label></td>
						<td><input id="organization_tree" class="easyui-combotree"  style="width:200px;"></td>
						<td></td>
						<td></td>
					</tr>	
					<tr>
						<td><label>机构名称:</label></td>
						<td><input type="text" name="name" value="${organization.name }" /></td>
						<td><label>机构LOGO:</label></td>
						<td><input type="file" name="file" id="organization_file"></td>
					</tr>	
				
					<tr>
						<td><label>资质:</label></td>
						<td><input type="text" name="honor" value="${organization.honor }" /></td>
						<td><label>创始人:</label></td>
						<td><input type="text" name="createUser" value="${organization.createUser }" /></td>
					</tr>	
					<tr>
						<td><label>规模:</label></td>
						<td>
							<select name="scale">
								<option value="0">10-30人</option>
								<option value="1">30-100人</option>
								<option value="2">100-500人</option>
								<option value="3">500人以上</option>
							</select>
						</td>
						<td><label>联系电话:</label></td>
						<td><input type="text" name="mobile" value="${organization.mobile}" /></td>
					</tr>
					<tr>
						<td><label>主营项目:</label></td>
						<td><input type="text" name="mainBusiness" value="${organization.mainBusiness }" /></td>
						<td><label>创始时间:</label></td>
						<td>
							<input class="easyui-datebox" type="text" value="${hx:toString(organization.ctime)  } "  name="ctime" style="width:150px"  />
						</td>
					</tr>
				</tbody>
			
			</table>
			<div id="dlg-buttons"  class="dialog-button" style="float: inherit;">
				<a href="javascript:;" class="easyui-linkbutton" id="organization_btnSave">${product.id == null ? '保存' : '修改' }</a>
				<a href="#" class="easyui-linkbutton" onclick="javascript:$('#organizationWin').window('close')">关闭</a>
			</div>
		</form:form>
<script type="text/javascript" src="${ctx }/script/jquery.form.js"></script>			
<script type="text/javascript">
$(function (){
	$('#organization_btnSave').on('click', function (){
		if($("#organization_file").val() == ''){
			$.messager.alert('错误提示',"请上传机构图标",'error');
			return ;
		}
		var val = $('#organization_tree').combotree('getValue');
		$('#organization_form input[name="parentId"]').val(val);
		$('#organization_form').ajaxSubmit({
				type : 'POST',
				url : '${ctx}/setting/organization/saveOrUpdate',
				dataType : 'json',
				success : function (data){
					if(data.code == 200){
						$('#organizationWin').window('close');
						$('#organization_DataGrid').datagrid('reload');
					}else{
						$.messager.alert('错误提示',data.message,'error');
					}
				}
			});
	});
	
	$('#organization_tree').combotree().combotree('loadData', ${trees});
	
});		

		
</script>		
