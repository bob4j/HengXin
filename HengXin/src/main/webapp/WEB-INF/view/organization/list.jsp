<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


   <table id="organization_DataGrid" class="easyui-treegrid" title="机构列表"  style="width:100%;height:800px"
			data-options="
				iconCls: 'icon-ok',
				rownumbers: true,
				animate: true,
				url: '${ctx }/setting/organization/list/data',
				method: 'get',
				idField: 'id',
				treeField: 'name',
				pagination: true,
				toolbar:'#organizationTab',
				loadMsg:'正在加载结构信息列表，请稍后...',
				onDblClickRow:organization_EditView
			">
		<thead>
			<tr>
				<th data-options="field:'name',width:180,editor:'text'">机构名称</th>
				<th data-options="field:'honor',width:60,align:'right',editor:'text'">资质</th>
				<th data-options="field:'createUser',width:80,editor:'text'">创始人</th>
				<th data-options="field:'mobile',width:120,editor:'text'">手机号码</th>
				<th data-options="field:'scale',width:80,editor:'text'">规模</th>
				<th data-options="field:'mainBusiness',width:80,editor:'text'">主营</th>
				<th data-options="field:'ctime',width:80,editor:'text'">创始时间</th>
				<th data-options="field:'createTime',width:80,editor:'text'">记录创建时间</th>
				<th data-options="field:'state',width:80,editor:'text'">状态</th>
			</tr>
		</thead>
	</table>
	<div id="organizationTab" style="height:auto">  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onClick="organization_AddView()">添加</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onClick="organization_DeleteAll()">删除</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onClick="organization_Reload()">刷新</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onClick="edit()">编辑</a> 
    </div> 
    
    
    <div id="organizationWin">
    
    </div>
    
    
<script type="text/javascript" src="${ctx }/script/moment/moment.js"></script>   
<script type="text/javascript" src="${ctx }/script/moment/zh-cn.js"></script>  
<script type="text/javascript" src="${ctx }/script/common/formToJson.js"></script>	
<jsp:include page="/WEB-INF/include/setup_ajax.jsp"></jsp:include>
<script>



function organization_AddView(){
		myWindow('organizationWin', '机构新增', '${ctx}/setting/organization/load', 800, 300);
}

function organization_EditView(){
	myWindow('organizationWin', '机构修改', '${ctx}/setting/organization/load', 800, 300);
}




</script>