<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%--  新增和修改弹出框 --%>
<script type="text/javascript" src="${ctx }/script/jquery.form.js"></script>
<link rel="stylesheet" href="${ctx }/css/common.css" type="text/css"></link>
		 <form:form action="" method="post" id="templateForm" enctype="multipart/form-data">
			<input type="hidden"  name="id" value="${template.id }">
			<table  class="table">
				<thead>
					<tr style="height: 50px;">
						<th colspan="4" >
							<h4>合同模板新增</h4>
						</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td><label>模板名称:</label></td>
						<td><input type="text" name="templateName" value="${template.templateName }" /></td>
						<td><label>请上传模板文件:</label></td>
						<td><input type="file" name="file" id="file_upload" /></td>
					</tr>	
					<tr>
						<td><label>设为默认:</label></td>
						<td>
							&nbsp;是<input type="radio" name="isDefualt" value="1" ${template.isDefualt ? 'checked' : '' }>
							否<input type="radio" name="isDefualt" value="0" ${template.isDefualt ? '' : 'checked' }>
						</td>
						<td></td>
						<td></td>
					</tr>	
					
				</tbody>
			
			</table>
			<div id="dlg-buttons"  class="dialog-button" style="float: inherit;">
				<a href="javascript:;" class="easyui-linkbutton" id="btnSave_template">保存</a>
				<a href="#" class="easyui-linkbutton" onclick="javascript:$('#btnSave_template').window('close')">关闭</a>
			</div>
		</form:form>
<script type="text/javascript">
$(function (){


	$('#btnSave_template').on('click', function (){
			$('#templateForm').ajaxSubmit({
				type : 'POST',
				url : '${ctx}/contract/template/saveOrUpdate',
				dataType : 'json',
				success : function (data){
					if(data.code == 200){
						$('#save_contract_Template').window('close');
						$('#template_contract_DataGrid').datagrid('reload');
					}else{
						$.messager.alert('错误提示',data.message,'error');
					}
				}
			});
	});
});				
</script>		
