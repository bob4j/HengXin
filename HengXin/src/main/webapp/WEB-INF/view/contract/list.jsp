<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


    <div  title="合同列表"  style="" border="false"> 
        <div class="easyui-panel"  border="false">
                <div region="north" border="false" style="height:50px;padding:10px;">
					<form action="" id="seachForm">
						<div style="text-align: center;">
							<input   style="width:300px;height: 25px" name="partyA" id="searchInput" placeholder="输入甲方进行搜索"></input>
							<select style="height: 25px" name="state">
								<option value="">合同状态</option>		
								<option value="1">未报</option>	
								<option value="2">已报</option>	
								<option value="3">未批</option>	
								<option value="4">资料上交</option>
								<option value="5">工程返利</option>			
							</select>
							<select style="height: 25px" name="isMode">
								<option value="">合同类型</option>	
								<option value="0">范本</option>		
								<option value="1">合同</option>	
							</select>
							<a href="javaScript:;" class="easyui-linkbutton" onclick="contractReload()" data-options="iconCls:'icon-search'">点击搜索</a>
						</div>	
					</form>
				</div>
                <div region="center" border="false" >
                	  <table class="easyui-datagrid"  id="contractList"
							data-options="url:'${ctx }/contract/list/data',fitColumns:true,nowrap:false,rownumbers:true,showFooter:true,pagination:true,
							loadMsg:'正在加载合同信息列表，请稍后...',toolbar:'#contractTb',checkbox:true,method:'get',onDblClickRow:contractEditView">
						<thead>
							<tr>
								<th data-options="field:'id',hidden:true"></th>
								<th data-options="field:'ck',width:80,checkbox:true"></th>
								<th data-options="field:'contractNo',width:80">合同编号</th>
								<th data-options="field:'partyA',width:250">甲方</th>
								<th data-options="field:'phoneA',width:80,align:'right'">甲方手机号码</th>
								<th data-options="field:'useFor',width:150,align:'right'">用途</th>
								<th data-options="field:'signedDate',width:80,sortable:true,formatter:formatDate,iconCls:'icon-sort'">合同签订日期</th>
								<th data-options="field:'totalMoney',width:60,align:'center'">合同总经费</th> 
								<th data-options="field:'instanStart',width:100,align:'center',sortable:true,formatter:formatDate">合同安装开始时间</th> 
								<th data-options="field:'instanEnd',width:100,align:'center',sortable:true,formatter:formatDate">合同安装结束时间</th> 
								<th data-options="field:'state',width:60,align:'center',formatter:formatState">合同状态</th> 
								<th data-options="field:'isMode',width:60,align:'center',formatter:formatMode">合同类型</th> 
							</tr>
						</thead>
					</table>
					<div id="contractTb" style="height:auto">  
				        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onClick="contractAddView()">添加</a>  
				        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onClick="contractDeleteAll()">删除</a>  
				        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onClick="contractReload()">刷新</a>  
				    </div> 
                </div>
             
         
    </div>
    </div>
    
<script type="text/javascript" src="${ctx }/script/moment/moment.js"></script>   
<script type="text/javascript" src="${ctx }/script/moment/zh-cn.js"></script>  
<script type="text/javascript" src="${ctx }/script/common/formToJson.js"></script>	
<jsp:include page="/WEB-INF/include/setup_ajax.jsp"></jsp:include>
<script>

//打开编辑页面
function contractEditView(rowIndex, rowData){
	 addTab4Framm("合同修改", "${ctx }/contract/load?id=" + rowData.id);
}


//删除
function contractDeleteAll(){
  	var json = $('#contractList').datagrid('getChecked');
  	$.ajaxUtils({
		data : JSON.stringify(json),
		url : '${ctx}/contract/deleteAll',
		contentType: "application/json; charset=utf-8",
		successMethod : function (data){
			$.messager.alert('提示','成功!');
			$('#contractList').datagrid('reload');
		}
	}); 
  }

//搜索框搜索功能
$('#searchInput').on('keypress', function (e){
	if(e.keyCode==13){
		reload();
		return false;
		
	}else{
		return true;
	}
});


//刷新数据表格
function contractReload(){
	var seachParams = $('#seachForm').serializeJSON();
	console.log(seachParams);
	$('#contractList').datagrid('options').queryParams = seachParams;
	$('#contractList').datagrid('reload');
}

function contractAddView(){
	 addTab4Framm("合同新增", "${ctx }/contract/load");
}


//格式化合同的状态   1.未报　2.已报　3.未批　　4.资料上交 　5.工程返利
function formatState(value){
	var result = "";
	switch (value) {
	case 1:
		result = "未报";
		break;
	case 2:
		result = "已报";
		break;
	case 3:
		result = "未批";
		break;
	case 4:
		result = "资料上交";
		break;
	case 5:
		result = "工程返利";
		break;
	default:
		result = "未知状态";
		break;
	}
	return result;
}

//格式化合同的类型    false:范本 true：合同
function formatMode(value){
	if(value){
		return "合同";
	}else{
		return "范本";
	}
}

</script>