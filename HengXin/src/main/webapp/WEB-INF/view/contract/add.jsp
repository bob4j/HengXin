<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	 <%@include file="/WEB-INF/include/tagLib.jsp" %>
	 
<html>
<head>	 
	 
	 <%@include file="/WEB-INF/include/easyUi.jsp" %>
<style>
.table{
	border-collapse:collapse;
	width: 90%;
	margin: 0 auto;
}
.table thead td{
	text-align: right;
}
.table td{
	border: 1px solid #ccc;
	background-color: #F1F6FF;
}
.table th{
	border-bottom: 1px solid #ccc;
}
.table td label, input{
	margin-left: 5px;
	margin-top: 5px;
	margin-bottom: 5px;
}
.table tfoot tr{
	height: 60px;
	text-align: right;
}
</style>


</head>
<body >
	
	<div title="${contract.id == null ? '合同新增' : '合同修改'}">
		<form action="" method="post" id="contractForm" onsubmit="return false;">
		<input type="hidden" name="id" value="${contract.id }">
        <table id="add_Table"  class="table"  >
			<thead style="text-align: center;">
				<tr style="height: 50px;">
					<th colspan="4" >
						<h4>格力空调工程项目购销合同</h4>
					</th>
				</tr>
				<tr style="height: 10px;">
					<td colspan="4" >
						<c:if test="${contract.id != null}">
							<select name="state">			
								<option value="1" ${contract.state == 1? 'selected="selected"' : ''}>未报</option>
								<option value="2" ${contract.state == 2? 'selected="selected"' : ''} >已报</option>
								<option value="3" ${contract.state == 3? 'selected="selected"' : ''}>未批</option>
								<option value="4" ${contract.state == 4? 'selected="selected"' : ''} >资料上交</option>
								<option value="5" ${contract.state == 5? 'selected="selected"' : ''} >工程返利</option>
							</select>
						</c:if>
						<select name="isMode">
							<option value="0" >范本</option>
							<option value="1" ${contract.isMode ? 'selected="selected"' : ''} >合同</option>
						</select>
					</td>
				</tr>
			</thead>        
        	<tbody >
        		<tr>
        			<td width="20%" ><label>甲方:</label></td>
        			<td ><input type="text" name="partyA" value="${contract.partyA }"/></td>
        			<td width="20%" ><label>合同编号:</label></td>
        			<td ><input type="text" name="contractNo" readonly="readonly" value="${ not empty contract.contractNo ? contract.contractNo : hx:getContractNo() }"/></td>
        		</tr>
        		<tr>
        			<td><label>乙方:</label></td>
        			<td><input type="text" name="partyB" value="${contract.partyB }" /></td>
        			<td><label>签订地点:</label></td>
        			<td><input type="text" name="signedAddre"  value="${contract.signedAddre }"/></td>
        		</tr>
        		<tr>
        			<td><label>空调用途:</label></td>
        			<td><input type="text" name="useFor"  value="${contract.useFor }"/></td>
        			<td><label>工程安装地点:</label></td>
        			<td><input type="text" name="instanAddre" value="${contract.instanAddre }" /></td>
        		</tr>
        		<tr>
        			<td><label>运输方式及费用负担:</label></td>
        			<td><input type="text" name="bearMethod" value="${contract.bearMethod }" /></td>
        			<td><label>超出原厂配置部分:</label></td>
        			<td><input type="text" name="chargeStandard" value="${contract.chargeStandard }" /></td>
        		</tr>
        		<tr>
        			<td><label>异议期限:</label></td>
        			<td><input type="text" name="objectionDate" value="${contract.objectionDate }" /></td>
        			<td><label>验收超时日期:</label></td>
        			<td><input type="text" name="checkDate" value="${contract.checkDate }" /></td>
        		</tr>
        		<tr>
        			<td><label>结算方式及期限:</label></td>
        			<td><input type="text" name="payMent" value="${contract.payMent }" /></td>
        			<td><label>产品合计:</label></td>
        			<td><input type="text" id="totalMoney" value="${contract.totalMoney }" /></td>
        		</tr>
        		<tr>
        			<td style="text-align: right;">
        				<a href="javascript:;" class="easyui-linkbutton" id="productSeach" data-options="iconCls:'icon-search'">产品选择</a>
        			</td>
        			<td colspan="3">
        				<select id="${contract.id == null ? 'contractSave' : 'contractUpdate' }"  style="width:80%"></select>
					</td>
        		</tr>
        		<tr style="height: 80px">
        			<td><label>违约责任:</label></td>
        			<td colspan="3"><textarea name="breachContract" style="width: 80%; height:100%; margin: 0 auto;"  type="text" >${contract.breachContract }</textarea></td>
        		</tr>
        		<tr style="height: 80px">
        			<td><label>其他约定事项:</label></td>
        			<td colspan="3"><textarea name="otherItem" style="width: 80%; height:100%; margin: 0 auto;" type="text" >${contract.otherItem }</textarea></td>
        		</tr>
        		<tr>
        			<td><label>甲方委托代理人:</label></td>
        			<td><input type="text" name="proxyA" value="${contract.proxyA }"/></td>
        			<td><label>乙方委托代理人:</label></td>
        			<td><input type="text" name="proxyB" value="${contract.proxyB }" /></td>
        		</tr>
        		<tr>
        			<td><label>甲方联系电话:</label></td>
        			<td><input type="text" name="telA" value="${contract.telA }" /></td>
        			<td><label>乙方联系电话:</label></td>
        			<td><input type="text" name="telB" value="${contract.telB }" /></td>
        		</tr>
        		<tr>
        			<td><label>甲方移动电话:</label></td>
        			<td><input type="text" name="phoneA" value="${contract.phoneA }" /></td>
        			<td><label>乙方移动电话:</label></td>
        			<td><input type="text" name="phoneB" value="${contract.phoneB }" /></td>
        		</tr>
        		<tr>
        			<td><label>合同签订日期:</label></td>
        			<td><input class="easyui-datebox" type="text"  name="signed" value="${hx:toString(contract.signedDate)  }"   /></td>
        			<td><label>安装时间:</label></td>
        			<td>
        				<input class="easyui-datebox" type="text" value="${hx:toString(contract.instanStart)  }"  name="start" style="width:150px"  />
        				至<input class="easyui-datebox" type="text" value="${hx:toString(contract.instanEnd) }"  name="end" style="width:150px"  />
        			</td>
        		</tr>
        	
        	</tbody>
        	<tfoot>
        		<tr>
        			<td colspan="4">
        				<div>
        					<button  href="#" class="easyui-linkbutton" style="border: 1px solid;" data-options="plain:true,iconCls:'icon-save'" id="${contract.id == null ? 'saveBtn' : 'updateBtn'}">${contract.id == null ? '保存' : '修改'}</button>
        					<button href="#" class="easyui-linkbutton" style="border: 1px solid;" data-options="plain:true,iconCls:'icon-cancel'">关闭</button>
        				</div>
        			</td>
        		</tr>
        	</tfoot>
        </table>
        </form>
    </div>
    
    
    <div id="productWin" class="easyui-window" data-options="title:'产品选择',iconCls:'icon-search',closed: true,modal: true" style="width:800px;height:430px;padding:5px;">
		<div class="easyui-layout" data-options="fit:true" >
			<div data-options="region:'center',border:false" id="productPanle" style="background:#fff;border:1px solid #ccc;width: 100%;height: 100%">
				
			</div>
			<div data-options="region:'south',border:false" style="text-align:right;padding:5px 0;">
				<a class="easyui-linkbutton" data-options="iconCls:'icon-ok'" href="javascript:void(0)" id="productChange">确认选择</a>
				<a class="easyui-linkbutton" data-options="iconCls:'icon-cancel'" href="javascript:void(0)" onclick="javascript:$('#productWin').window('close');">取消选择</a>
			</div>
		</div>
	</div>

<script type="text/javascript" src="${ctx }/script/common/ajaxUtils.js"></script>	
<script type="text/javascript" src="${ctx }/script/common/formToJson.js"></script>	
<jsp:include page="/WEB-INF/include/setup_ajax.jsp"></jsp:include>
<script type="text/javascript">



	
$(function (){

	var changeAll = '${products}';


	var productTable = $('#${contract.id == null ? 'contractSave' : 'contractUpdate' }').combogrid({
			panelWidth:800,
			value:'已选择产品列表',
			columns:[[
				{field:'id',title:'ID',hidden:true,width:60},	
				{field:'number',title:'产品编号',width:60},
				{field:'standard',title:'产品规格',width:120},
				{field:'voltage',title:'电压',width:100},
				{field:'unit',title:'单位',width:100},
				{field:'stock',title:'产品库存',width:100},
				{field:'stockPrice',title:'进货单价',width:100},
				{field:'salePrice',title:'销售价格',width:100},
				{field:'stockNum',title:'进货数量',width:100}
			]]
		});


	$('#productSeach').on('click', function (){
		$('#productWin').window('open');
		$('#productPanle').panel({
			href : '${ctx }/product/list?flag=productChange'
		});		
	});

	

	$('#${contract.id == null ? 'saveBtn' : 'updateBtn'}').on('click', function (){
		var contract = $('#contractForm').serializeJSON();
		if(changeAll != ''){
			contract.product = changeAll;
		}
		$.ajaxUtils({
			data : JSON.stringify(contract),
			url : '${ctx}/contract/saveOrUpdate',
			contentType: "application/json; charset=utf-8",
			async : true,
			successMethod : function (){
				$.messager.alert('提示','${contract.id == null ? '保存成功' : '修改成功'}');
				/* $.messager.confirm('提示信息','操作成功！本页即将关闭',function(r){
					closeTab("${contract.id == null ? '合同新增' : '合同修改'}");
				}); */
			}
		});	
		
	});
	
	if(changeAll != ''){
		changeAll = $.parseJSON(changeAll);
		productTable.combogrid("grid").datagrid("loadData", changeAll);
	}
	
	//产品选择
	$('#productChange').on('click', function (){
		changeAll = $('#productDataGrid').datagrid('getChecked');
		var salePrices = 0;
		$.each(changeAll, function (i, o){
			if(!isNaN(o.salePrice)){
				salePrices += o.salePrice;
			}
		});
		productTable.combogrid("grid").datagrid("loadData", changeAll);
		$('#totalMoney').val(salePrices.toFixed(2));
		$('#productWin').window('close');
		
	
	});

});

</script>   
</body>
</html>
