<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>



    <table class="easyui-datagrid"  id="template_contract_DataGrid"
			data-options="url:'${ctx }/contract/template/list/data',fitColumns:true,nowrap:false,rownumbers:true,showFooter:true,pagination:true,
			loadMsg:'正在加载合同模板信息列表，请稍后...',toolbar:'#template_contract_Tb',checkbox:true,method:'get',onDblClickRow:template_contract_EditView,pageSize:20">
		<thead>
			<tr>
				<th data-options="field:'id',hidden:true"></th>
				<th data-options="field:'ck',width:80,checkbox:true"></th>
				<th data-options="field:'templateName',width:80">合同模板名称</th>
				<th data-options="field:'fileType',width:120">文件格式</th>
				<th data-options="field:'isDefualt',width:250">是否默认</th>
				<th data-options="field:'createTime',width:80,align:'right'">创建时间</th>
			</tr>
		</thead>
	</table>
	<div id="template_contract_Tb" style="height:auto">  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onClick="template_contract_AddView()">上传合同模板</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onClick="template_contract_DeleteAll()">删除</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onClick="template_contract_Reload()">刷新</a>  
    </div> 
	
	
	<div id="save_contract_Template">
	</div> 
	

<jsp:include page="/WEB-INF/include/setup_ajax.jsp"></jsp:include>	
<script type="text/javascript">

  function template_contract_AddView(){
  	myWindow('save_contract_Template', '合同模板新增', '${ctx}/contract/template/load', 800, 300);
  }
  
  function template_contract_EditView(rowIndex, rowData){
  		myWindow('save_contract_Template', '合同模板修改', '${ctx}/contract/template/load?id=' + rowData.id, 800, 300);
  }
  
  
  function template_contract_DeleteAll(){
  	var json = $('#template_contract_DataGrid').datagrid('getChecked');
  	$.ajaxUtils({
		data : JSON.stringify(json),
		url : '${ctx}/contract/template/deleteAll',
		contentType: "application/json; charset=utf-8",
		successMethod : function (data){
			$.messager.alert('提示','成功!');
			$('#template_contract_DataGrid').datagrid('reload');
		}
	}); 
  }

 function template_contract_Reload(){
 	$('#template_contract_DataGrid').datagrid('reload');
 }

	


 
</script>	
