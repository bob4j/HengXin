<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>



    <table class="easyui-datagrid"  id="userm_DataGrid"
			data-options="url:'${ctx }/userm/list/data',fitColumns:true,nowrap:false,rownumbers:true,showFooter:true,pagination:true,
			loadMsg:'正在加载用户信息列表，请稍后...',toolbar:'#userm_tb',checkbox:true,method:'get'">
		<thead>
			<tr>
				<th data-options="field:'userId',hidden:true"></th>
				<th data-options="field:'ck',width:80,checkbox:true"></th>
				<th data-options="field:'userName',width:120">用户名</th>
				<th data-options="field:'currentIp',width:120">当前登录IP</th>
				<th data-options="field:'lastIp',width:80,align:'right'">上一次登录IP</th>
				<th data-options="field:'currentLoginTime',width:80,align:'right',formatter:formatDate">本次登录时间</th>
				<th data-options="field:'lastLoginTime',width:250,formatter:formatDate">上一次登录时间</th>
				<th data-options="field:'createTime',width:60,align:'center',formatter:formatDate">日志创建时间</th> 
			</tr>
		</thead>
	</table>
	<div id="userm_tb" style="height:auto">  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onClick="userm_addView()">添加用户</a>  
     <!--    <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onClick="deleteAll()">删除日志</a>  --> 
    </div> 
	
	
	<div id="userm_Win">
	</div> 
	

<script type="text/javascript" src="${ctx }/script/moment/moment.js"></script>   
<script type="text/javascript" src="${ctx }/script/moment/zh-cn.js"></script>  	
<script type="text/javascript">

  function userm_addView(){
  	myWindow('userm_Win', '用户新增', '${ctx}/userm/load', 800, 300);
  }
  
  
  




 
</script>	
