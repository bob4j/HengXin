<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%--  新增和修改弹出框 --%>
<link rel="stylesheet" href="${ctx }/css/common.css" type="text/css"></link>
	
		 <form:form action="" method="post" id="userm_Form">
		 	
			<table  class="table">
				<thead>
					<tr style="height: 50px;">
						<th colspan="4" >
							<h4>用户新增</h4>
						</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td><label>用户名称:</label></td>
						<td><input type="text" name="name" /></td>
						<td><label>用户状态:</label></td>
						<td>
							<select name="userState">
								<option value="0"  >正常状态</option>
								<option value="1" >冻结状态</option>
							</select>
						</td>
					</tr>	
					<tr>
						<td><label>账号:</label></td>
						<td><input type="text" name="account"  /></td>
						<td><label>年龄:</label></td>
						<td> <input type="text" name="age" /> </td>
					</tr>
					<tr>
						<td><label>邮箱:</label></td>
						<td><input type="text" name="email"  /></td>
						<td><label>密码:</label></td>
						<td><span>*初始密码为123456</span> </td>
					</tr>
					<tr>
						<td><label>选择组织:</label></td>
						<td >
							<input id="userm_organization_tree" class="easyui-combotree" name="organizationId"  style="width:80%;">
						</td>
						<td><label>选择身份:</label></td>
						<td >
							<input id="userm_role_tree" class="easyui-combotree" name="roleId" data-options="idField:'id', textField:'text'"   style="width:80%;">
						</td>
					</tr>
					
				</tbody>
			
			</table>
			<div id="dlg-buttons"  class="dialog-button" style="float: inherit;">
				<a href="javascript:;" class="easyui-linkbutton" id="userm_btnSave">保存</a>
				<a href="#" class="easyui-linkbutton" onclick="javascript:$('#userm_Win').window('close')">关闭</a>
			</div>
		</form:form>
<script type="text/javascript" src="${ctx }/script/common/formToJson.js"></script>
<jsp:include page="/WEB-INF/include/setup_ajax.jsp"></jsp:include>			
<script type="text/javascript">

$(function (){
	$('#userm_btnSave').on('click', function (){
		var role_form = $('#userm_Form').serializeJSON();
		 $.ajaxUtils({
			data : JSON.stringify(role_form),
			url : '${ctx}/userm/save',
			async : true,
			contentType: "application/json; charset=utf-8",
			successMethod : function (data){
				$('#userm_Win').window('close');
				$('#userm_DataGrid').datagrid('reload');
			}
		}); 
	});
	
	
	$('#userm_organization_tree').combotree().combotree('loadData', ${organizationTree}).combotree({
		
		onSelect : function (record){
			$('#userm_role_tree').combotree({
				url:'${ctx }/setting/role/combotree',
				method: 'get',
				idField:'id',
				textField:'text',
				queryParams : {'organizationId' : record.id}
			});
		}
		
		
	});
	
});	

			
</script>		
