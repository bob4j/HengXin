<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
  <%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>  
<%--  新增和修改弹出框 --%>
vt
<link rel="stylesheet" href="${ctx }/css/common.css" type="text/css"></link>
	
		 <form:form action="" method="post" id="menu_form" enctype="multipart/form-data">
			<input type="hidden" value="${menu.id }" name="id">
			<input type="hidden" value="" name="parentId">
			<table  class="table">
				<thead>
					<tr style="height: 50px;">
						<th colspan="4" >
							<h4>${menu.id == null ? '菜单新增' : '菜单修改' }</h4>
						</th>
					</tr>
				</thead>
				
				<tbody>
					<tr>
						<td><label>菜单父级:</label></td>
						<td><input id="menu_tree" name="" class="easyui-combotree" data-options="url:'${ctx }/setting/menu/combotree',method: 'get',idField:'id',textField:'text',value:'${menu.parentId}'" style="width:200px;"></td>
						<td><label>菜单名称:</label></td>
						<td><input type="text" name="name" value="${menu.name }"></td>
					</tr>	
					<tr>
						<td><label>菜单地址:</label></td>
						<td><input type="text" name="uri" value="${menu.uri }" /></td>
						<td><label>菜单图标:</label></td>
						<td>
							<input type="file" name="file">
						</td>
					</tr>	
				
					<tr>
						<td><label>菜单标示符:</label></td>
						<td><input type="text" name="permission" value="${menu.permission }" /></td>
						<td><label>排序:</label></td>
						<td><input type="text" name="orderField" value="${menu.orderField }" /></td>
					</tr>	
					<tr>
						<td><label>菜单描述:</label></td>
						<td colspan="3">
							<textarea  style="width: 80%; height:100%; margin: 0 auto;" >${menu.mateDec }</textarea>	
						</td>
					</tr>
				</tbody>
				
			
			</table>
			<div id="dlg-buttons"  class="dialog-button" style="float: inherit;">
				<a href="javascript:;" class="easyui-linkbutton" id="menu_btnSave">${menu.id == null ? '保存' : '修改' }</a>
				<a href="#" class="easyui-linkbutton" onclick="javascript:$('#menu_Win').window('close')">关闭</a>
			</div>
		</form:form>
<script type="text/javascript" src="${ctx }/script/jquery.form.js"></script>		
<script type="text/javascript">
$(function (){
	$('#menu_btnSave').on('click', function (){
		var val = $('#menu_tree').combotree('getValue');
		$('#menu_form input[name="parentId"]').val(val);
		$('#menu_form').ajaxSubmit({
				type : 'POST',
				url : '${ctx}/setting/menu/saveOrUpdate',
				dataType : 'json',
				success : function (data){
					if(data.code == 200){
						$('#menu_Win').window('close');
						$('#menu_treegrid').treegrid('reload');
					}else{
						$.messager.alert('错误提示',data.message,'error');
					}
				}
			});
	});
	
	
});		






		
</script>		
