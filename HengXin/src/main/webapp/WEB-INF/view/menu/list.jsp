<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


   <table id="menu_treegrid" class="easyui-treegrid" title="机构列表"  style="width:100%;height:800px"
			data-options="
				iconCls: 'icon-ok',
				rownumbers: true,
				animate: true,
				url: '${ctx }/setting/menu/list/data',
				method: 'get',
				idField: 'id',
				treeField: 'name',
				pagination: true,
				toolbar:'#menu_Tab',
				loadMsg:'正在加载菜单信息列表，请稍后...',
				onDblClickRow:menu_EditView
			">
		<thead>
			<tr>
				<th data-options="field:'id',hidden:true"></th>
				<th data-options="field:'ck',width:80,checkbox:true"></th>
				<th data-options="field:'name',width:180,editor:'text'">菜单名称</th>
				<th data-options="field:'uri',width:60,align:'right',editor:'text'">菜单链接</th>
				<th data-options="field:'logo',width:80,editor:'file'">菜单图标</th>
				<th data-options="field:'permission',width:120,editor:'text'">菜单可操作标示符</th>
				<th data-options="field:'mateDec',width:80,editor:'text'">菜单描述</th>
				<th data-options="field:'orderField',width:80,editor:'text'">排序</th>
				<th data-options="field:'parentId',width:80,editor:'text'">父级菜单名称</th>
				<th data-options="field:'delState',width:80,editor:'text'">删除状态</th>
			</tr>
		</thead>
	</table>
	<div id="menu_Tab" style="height:auto">  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-add',plain:true" onClick="menu_AddView()">添加</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-remove',plain:true" onClick="menu_DeleteAll()">删除</a>  
        <a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-reload',plain:true" onClick="menu_Reload()">刷新</a>  
    </div> 
    
    
    <div id="menu_Win">
    
    </div>
    
    
<script type="text/javascript" src="${ctx }/script/moment/moment.js"></script>   
<script type="text/javascript" src="${ctx }/script/moment/zh-cn.js"></script>  
<script type="text/javascript" src="${ctx }/script/common/formToJson.js"></script>	
<jsp:include page="/WEB-INF/include/setup_ajax.jsp"></jsp:include>
<script>




function menu_AddView(){
		myWindow('menu_Win', '菜单新增', '${ctx}/setting/menu/load', 800, 300);
}

function menu_EditView( rowData){
	myWindow('menu_Win', '菜单修改', '${ctx}/setting/menu/load?id=' + rowData.id, 800, 300);
}


function menu_Reload(){
	$('#menu_treegrid').treegrid('reload');
}	

 function menu_DeleteAll(){
  	var json = $('#menu_treegrid').treegrid('getChecked');
  	$.ajaxUtils({
		data : JSON.stringify(json),
		url : '${ctx}/setting/menu/deleteAll',
		contentType: "application/json; charset=utf-8",
		successMethod : function (data){
			$.messager.alert('提示','成功!');
			$('#menu_treegrid').treegrid('reload');
		}
	}); 
  }



</script>